<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.layout.login');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(array('prefix' => 'dashboard','as'=>'admin.' ,'middleware' => 'auth'), function() {

    Route::get('/', 'indexController@index')->name('layout.main');

    Route::resource('users', 'userController');
    Route::resource('guarantors', 'guarantorController');
    Route::resource('clients', 'clientController');
    Route::resource('mediators', 'mediatorController');
    Route::resource('settings', 'settingController');

    Route::resource('clearances', 'clearanceController');
    Route::resource('car_contracts', 'car_contractController');
    Route::resource('card_contracts', 'card_contractController');
    Route::any('/card_sadad/{id}','card_contractController@sadad' )->name('card_contracts.sadad');

    Route::post('/sadad-qests/{id}','card_contractController@qest_sadad' )->name('card_contracts.sadad_qests');
    Route::any('/sadad-cars/{id}','car_contractController@qest_sadad' )->name('car_contracts.sadad_qests');


    Route::get('/card','card_contractController@qast' )->name('card_contracts.qests');

    Route::get('/sadad/{id}','car_contractController@sadad' )->name('car_contracts.sadad');
    Route::get('/card_report/{id}','card_contractController@report' )->name('card_contracts.report');

    Route::get('/report/{id}','car_contractController@report' )->name('car_contracts.report');
    Route::get('/contract/{id}','car_contractController@print' )->name('car_contracts.print');
    Route::get('/card_contract/{id}','card_contractController@print' )->name('card_contracts.print');

    Route::get('/client_sanad/{id}','car_contractController@client_sanad' )->name('car_contracts.client_sanad');
    Route::get('/guarantor_sanad/{id}','car_contractController@guarantor_sanad' )->name('car_contracts.guarantor_sanad');

    Route::get('/card_client_sanad/{id}','card_contractController@client_sanad' )->name('card_contracts.client_sanad');
    Route::get('/card_guarantor_sanad/{id}','card_contractController@guarantor_sanad' )->name('card_contracts.guarantor_sanad');
    Route::any('/collected_premiums','ReportController@premiums')->name('reports.collected_premiums');

});


