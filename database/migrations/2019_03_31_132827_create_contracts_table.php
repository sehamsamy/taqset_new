<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('client_id');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade')->nullable();


            $table->Integer('guarantor_id');



            $table->Integer('mediator_id');


            $table->enum('type',['car','card']);
            $table->enum('kind',['client','guarantor'])->default('client');

            //car
            $table->string('car_name')->nullable();
            $table->string('shas_num')->nullable();
            $table->string('car_num')->nullable();
            $table->string('color')->nullable();
            $table->string('model')->nullable();

            //card
            $table->integer('number')->nullable();
            $table->double('class')->nullable();

            $table->double('paid_amount')->nullable();
            $table->double('amount')->nullable();
            $table->double('added_value')->nullable();
            $table->integer('months_count')->nullable();

            $table->string('notes');



            $table->date('start_date');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
