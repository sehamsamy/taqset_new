<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMediatotToContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts', function (Blueprint $table) {


            $table->unsignedInteger('guarantor_id');
            $table->foreign('guarantor_id')->references('id')->on('guarantors')->onDelete('cascade')->nullable();

            $table->unsignedInteger('mediator_id');
            $table->foreign('mediator_id')->references('id')->on('mediators')->onDelete('cascade')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts', function (Blueprint $table) {
            //
        });
    }
}
