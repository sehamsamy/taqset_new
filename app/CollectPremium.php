<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollectPremium extends Model
{
    protected $fillable = [
        'payment_id','date','amount'
    ];
    protected $table ='collect_premiums';

    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }
    
}
