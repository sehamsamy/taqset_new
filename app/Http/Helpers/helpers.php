<?php



/**
 * Setting Name
 * @param $name
 * @return mixed
 */



function getsetting($name)
{
    $setting=\App\Setting::where('name',$name)->first();
    if (!$setting) return "";
    return $setting->value();

}
/**
 * Upload Path
 * @return string
 */
function uploadpath()
{
    return 'photos';
}



/**
 * Get Image
 * @param $filename
 * @return string
 */
function getimg($filename)
{
    $base_url = url('/');
    return $base_url.'/storage/'.$filename;
}

/**
 * Upload an image
 * @param $img
 */
function uploader($request,$img_name)
{
    $path = \Storage::disk('public')->putFile(uploadpath(),$request->file($img_name));
    return $path;
}


function status()
{
    $array = [
        '1'=>'مفعل',
        '0'=>'غير مفعل',
    ];
    return $array;
}


function cartStatus()
{
    $array = [
        'completed'=>'منتهيه',
        'opening'=>'قيد التنفيذ',
        'rejected'=>'مرفوضة',
    ];
    return $array;
}


function cities()
{
    $cities = App\City::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['ar_name']];
    });
    return $cities;
}


function companies()
{
    $cities = App\Company::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['title'].' : '.$item['commercial_no']];
    });
    $cities[0]='اخرى';
    return $cities;
}

function employees()
{
    $cities = App\Employee::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['name'].'  ('.$item['border_no'].')'];
    });

    return $cities;
}

function hijriCast($date){
    \GeniusTS\HijriDate\Date::setTranslation(new \GeniusTS\HijriDate\Translations\Arabic());

    return        \GeniusTS\HijriDate\Hijri::convertToGregorian( Carbon\Carbon::parse($date)->format('d'),  Carbon\Carbon::parse($date)->format('m'),  Carbon\Carbon::parse($date)->format('Y'))->format('Y-m-d');
}


function group()
{
    $cities = App\Group::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['name']];
    });
    return $cities;
}function fee_category()
{
    $cities = App\feeCategory::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['title']];
    });
    return $cities;
}
function products()
{
    $products = App\Product::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['title']];
    });
    return $products;
}

function type()
{
    $cities = App\RestType::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['name']];
    });
    return $cities;
}

function popup($name, $validator = null,$custom = null)
{
    if ($validator != null) {
        return alert()->error($validator->errors()->first())->autoclose(5000);
    }
    if ($name == 'add') {
        return alert()->success('تم الاضافه بنجاح')->autoclose(5000);
    } if ($name == 'verify') {
    return alert()->success('تم الاضافه وبإنتظار المراجعة')->autoclose(5000);
} elseif ($name == 'update') {
    return alert()->success('تم التعديل بنجاح')->autoclose(5000);
}elseif ($name == 'delete'){
    return alert()->success('تم الحذف بنجاح')->autoclose(5000);
}elseif ($name == 'custom'){
    return alert()->success($custom)->autoclose(5000);
}elseif ($name == 'info'){
    return alert()->info($custom)->autoclose(5000);
}elseif ($name == 'danger'){
    return alert()->error($custom)->autoclose(5000);
}

}

function Activity($text){
    $input['user_id']=auth()->id();
    $input['text']=$text;
    $activity=App\Activity::create($input);
    //dd($activity);
}

function countries()
{
    $countries = App\Country::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['ar_name']];
    });
    return $countries;
}

function notifyGeneral($message,$users,$cart)
{
    $data = ['cart_id'=>$cart->id,'status'=>$cart->status,'type'=>'cart'];


    foreach ($users as $user)
    {
        $user->notify(new \App\Notifications\GeneralNotifications($user,$message,$cart));
    }
}


function GenerateCode() {
    $code = str_random(6); // better than rand()
    // call the same function if the barcode exists already
    if (UniqueCode($code)) {
        return GenerateCode();
    }
    // otherwise, it's valid and can be used
    return $code;
}

function UniqueCode($code)
{
    return \App\Coupon::where('code',$code)->first();
}

function multiUploader($request,$img_name,$onId=null){
    $images= [];
    $i = 0;
    foreach ($request[$img_name] as $image){
        $path = \Storage::disk('public')->putFile(uploadpath(), $image);
        $images[$i] = $path;
        $i++;
    }
    return $images;
}

function licenseSave($model,$crud,$request=null,$item_id=null)
{

    if (!empty($request))
        $request = serialize($request);
    else
        $request = null;

    \App\License::create([
        'model'=>$model,
        'item_id'=>$item_id,
        'crud'=>$crud,
        'data'=>$request,
        'user_id'=>auth()->id()
    ]);
}

function licenseOps($license_id)
{
    $license = \App\License::find($license_id);

    $crud = $license->crud;

    $model = $license->model;

    $item_id = $license->item_id;

    $data = unserialize($license->data);

    if ($crud != 'delete')
    {
        if ($crud == 'update')
            $model::find($item_id)->$crud($data);
        else
            $model::$crud($data);
    }else{
        $model::find($item_id)->$crud();

    }
    $license->delete();
}
