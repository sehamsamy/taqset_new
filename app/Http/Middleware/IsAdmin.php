<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (!auth()->check()) {
            return redirect()->route('login');
        }
        if (auth()->check() ) {
            \auth()->logout();
            abort(401);
        }
        return $next($request);
    }

}
