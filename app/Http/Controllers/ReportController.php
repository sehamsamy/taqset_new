<?php

namespace App\Http\Controllers;

use App\CollectPremium;
use Illuminate\Http\Request;

class ReportController extends Controller
{

    public function premiums(Request $request){
        // dd($request->all());
        if ($request->has('from') && $request->has('to')) {


            $premiums=CollectPremium::whereBetween('created_at',[$request['from'],$request['to']])->get();
        
        }else{
        $premiums=CollectPremium::all();
        }
        return view('admin.reports.collected_premiums',compact('premiums'));
    }
}
