<?php

namespace App\Http\Controllers;

use App\Activity;
use App\City;
use App\User;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index')->with('users',User::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.users.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //dd($request->all());
        $this->validate($request,[
            'name'=>'required|string|',
            'email'=>'nullable|string|email|max:255|unique:users',


        ]);
        $inputs=$request->all();
        $inputs['password']=bcrypt($inputs['password']);

        User::create($inputs);

        alert()->success('تم اضافة المستخدم بنجاح !')->autoclose(5000);
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $activites=Activity::where('user_id',$id)->get();
        return view('admin.users.view',compact('activites'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user=User::find($id);
        return view('admin.users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user=User::find($id);
        $this->validate($request,[
            'name'=>'required|string|',
            'email'=>'required|string|email|max:255|unique:users,email,'.$user->id,

        ]);
        $inputs=$request->all();
        if($request->password != null) {$user->update(['password'=>bcrypt($request->password),]);}

        $user->update(array_except($inputs,['password']));


        alert()->success('تم تعديل  المستخدم بنجاح !')->autoclose(5000);
        return redirect('dashboard/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { //dd($id);
        $user=User::find($id);
        if($user->email=='admin@admin.com'){
            alert()->error('لايمكن حذف الادمن  ');
            return back();
        }
        if ($user){
            $user->delete();
            alert()->success('تم حذف المستخدم  بنجاح');
            return back();
        }
        alert()->error('المستخدم   الذى تحاول حذفه غير موجود');
        return back();
    }
}
