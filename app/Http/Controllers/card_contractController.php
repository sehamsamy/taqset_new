<?php

namespace App\Http\Controllers;

use App\Activity;
use App\City;
use App\Client;
use App\CollectPremium;
use App\Contract;
use App\Guarantor;
use App\Http\Helpers\num_to_ar;
use App\Mediator;
use App\Payment;
use App\User;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use GeniusTS\HijriDate\Date;

class card_contractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.card_contracts.index')->with('card_contracts',Contract::where('type','card')->orderByDesc('created_at')->get());
    }

    public function qast(){

        $futureday= date('Y-m-d', strtotime("+15 days"));
        //  dd($futureday);
        $qests=Payment::where('date','<=',$futureday)->where('pay',0)->get();

        return view('admin.card_contracts.view',compact('qests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $guarantors=Guarantor::pluck('name','id')->toArray();
        $clients=Client::pluck('name','id')->toArray();
        $mediators=Mediator::pluck('name','id')->toArray();

        return view('admin.card_contracts.add',compact('guarantors','clients','mediators'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request->all());
        $this->validate($request,[



        ]);
        $inputs=$request->all();
        $user_id=Auth::user()->id;
        $contract= Contract::create($inputs);
        $client_id=$inputs['client_id'];
        $start_date=$inputs['start_date'];

        $class=$inputs['class'];
        $number=$inputs['number'];
        $added_value=$inputs['added_value'];
        $months_count=$inputs['months_count'];
        $amount=$class*$number;
        $total_amount=$amount+$added_value;
        $onepay=($amount+$added_value)/$months_count;



        $contract->update([
            'type'=>'card',
            'amount'=>$amount
        ]);


        for ($i=0;$i<$months_count;$i++)

        {
            Payment::create([
                'user_id'=>$user_id,
                'client_id'=>$client_id,
                'contract_id'=>$contract->id,
                'one_pay'=>$onepay,
                'amount'=>$total_amount,
                'date'=>$start_date
            ]);
            $total_amount=$total_amount-$onepay;
            $start_date=Carbon::parse($start_date)->addMonths(1);

              }
        $text=' اضافه عقد  بطاقات '. $contract->id;
        Activity($text);

        alert()->success('تم اضافة العقد بنجاح !')->autoclose(5000);
//        return back();
/////////////////////////////////////////////////////////////
        $numbers=new num_to_ar(($contract->class*$contract->number)+$contract->added_value,"male");

        $total=$numbers->convert_number().'ريال';
        $contract_date1= $contract->created_at;

        $today_date = $contract_date1->format('d-m-Y');
        $payment=Payment::where('contract_id',$contract->id)->first();
        $numbersx=new num_to_ar($payment->one_pay,"male");
        $one_pay=$numbersx->convert_number().'ريال';
        $date = \GeniusTS\HijriDate\Hijri::convertToHijri($today_date)->format('d-m-Y');
        $dayname = $contract_date1->locale('ar')->dayName;
        // dd($day);
      /*  if ($day=='Sat') {
            $dayname = 'السبت';
        }else  if ($day=='Sun') {
            $dayname = 'الاحد';
        }else  if ($day=='Mon') {
            $dayname = 'الاثنين';
        }else  if ($day=='Tue') {
            $dayname = 'الثلاتاء';
        }else  if ($day=='Wed') {
            $dayname = 'الاربعاء';
        }else  if ($day=='Thu') {
            $dayname = 'الخميس';
        }else  if ($day=='Fri') {
            $dayname = 'الجمعة';
        }*/

//dd($dayname);

        return view('admin.card_contracts.contract',compact('total','one_pay','today_date','contract','date','payment','dayname','payment'));



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $qests=Payment::where('contract_id',$id)->get();
        return view('admin.card_contracts.view',compact('qests'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $contract=Contract::find($id);
        $guarantors=Guarantor::pluck('name','id')->toArray();
        $mediators=Mediator::pluck('name','id')->toArray();
        $clients=Client::pluck('name','id')->toArray();
        return view('admin.card_contracts.edit',compact('clients','guarantors','mediators','contract'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       // dd($request->all());
        $contract=Contract::find($id);

        $inputs=$request->all();
        $contract->update($inputs);
        $text=' تعديل العقد '. $contract->id;
        Activity($text);
        alert()->success('تم تعديل  العقد بنجاح !')->autoclose(5000);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $contract=Contract::find($id);
        $contract->delete();

        $text=' حذف العقد '. $contract->id;
        Activity($text);
        alert()->success('تم حذف العقد بنجاح');
        return back();
    }
    public function sadad(Request $request,$id)
    {
        $payment=Payment::find($id);
       $payment->update([
            'pay'=>1,
           'updated_at'=>Date::today()->format('d-m-Y')
        ]);
        $qests=Payment::where('contract_id',$id)->get();
//        return back();

CollectPremium::create([
    'date'=>Date::today()->format('d-m-Y'),
    'payment_id'=>$payment->id,
    'amount'=>$payment->amount,
]);
        $qest_num=$request['key'];

        return view('admin.card_contracts.sadad',compact('payment','qest_num'));


    }

    public function report($id)
    {

        $contract=Contract::find($id);
        $paymemts_paid=Payment::where('contract_id',$id)->where('pay',1)->count();
        $paymemts_notpaid=Payment::where('contract_id',$id)->where('pay',0)->count();
        $payments=Payment::where('contract_id',$id)->where('pay',0)->get();
        $paid_1=Payment::where('contract_id',$id)->where('pay',1)->sum('one_pay');
        $paid=ceil($paid_1);
        $not_paid_1=Payment::where('contract_id',$id)->where('pay',0)->sum('one_pay');
        $not_paid=ceil($not_paid_1);



        return view('admin.card_contracts.report',compact('contract','paymemts_paid','paymemts_notpaid','payments','paid','not_paid'));

    }
    public function print($id){
        $contract=Contract::find($id);
        $numbers=new num_to_ar(($contract->class*$contract->number)+$contract->added_value,"male");

        $total=$numbers->convert_number().'ريال';
        $contract_date1= $contract->created_at;

        $today_date = $contract_date1->format('d-m-Y');
        $payment=Payment::where('contract_id',$contract->id)->first();
        $numbersx=new num_to_ar($payment->one_pay,"male");
        $one_pay=$numbersx->convert_number().'ريال';
        $date = \GeniusTS\HijriDate\Hijri::convertToHijri($today_date)->format('d-m-Y');
        $dayname = $contract_date1->locale('ar')->dayName;
      // dd($day);
      /*  if ($day=='Sat') {
            $dayname = 'السبت';
        }else  if ($day=='Sun') {
            $dayname = 'الاحد';
        }else  if ($day=='Mon') {
            $dayname = 'الاثنين';
        }else  if ($day=='Tue') {
            $dayname = 'الثلاتاء';
        }else  if ($day=='Wed') {
            $dayname = 'الاربعاء';
        }else  if ($day=='Thu') {
            $dayname = 'الخميس';
        }else  if ($day=='Fri') {
            $dayname = 'الجمعة';
        }*/

//dd($dayname);

        return view('admin.card_contracts.contract',compact('total','one_pay','today_date','contract','date','payment','dayname','payment'));
    }
    public  function guarantor_sanad($id){

        $contract=Contract::find($id);
        $numbers=new num_to_ar(($contract->class*$contract->number)+$contract->added_value,"male");
        $total=$numbers->convert_number().'ريال';

        $contract_date1= $contract->created_at;

        $today_date = $contract_date1->format('d-m-Y');
        $payment=Payment::where('contract_id',$contract->id)->first();
        $numbersx=new num_to_ar($payment->one_pay,"male");
        $one_pay=$numbersx->convert_number().'ريال';
        $date = \GeniusTS\HijriDate\Hijri::convertToHijri($today_date)->format('d-m-Y');
        $day = $contract_date1->format('D');
                $dayname = $contract_date1->locale('ar')->dayName;

        //    dd($payment);
   /*     if ($day=='Sta') {
            $dayname = 'السبت';
        }else  if ($day=='Sun') {
            $dayname = 'الاحد';
        }else  if ($day=='Mon') {
            $dayname = 'الاثنين';
        }else  if ($day=='Tue') {
            $dayname = 'الثلاتاء';
        }else  if ($day=='Wen') {
            $dayname = 'الاربعاء';
        }else  if ($day=='Thr') {
            $dayname = 'الخميس';
        }else  if ($day=='Fri') {
            $dayname = 'الجمعة';
        }*/
        return view('admin.card_contracts.guarantor_sanad',compact('total','one_pay','today_date','contract','date','payment','dayname','payment'));

    }

    public  function client_sanad($id){


        $contract=Contract::find($id);
        $numbers=new num_to_ar(($contract->class*$contract->number)+$contract->added_value,"male");
        $total=$numbers->convert_number().'ريال';

        $contract_date1= $contract->created_at;

        $today_date = $contract_date1->format('d-m-Y');
        $payment=Payment::where('contract_id',$contract->id)->first();
        $numbersx=new num_to_ar($payment->one_pay,"male");
        $one_pay=$numbersx->convert_number().'ريال';
        $date = \GeniusTS\HijriDate\Hijri::convertToHijri($today_date)->format('d-m-Y');
        $dayname = $contract_date1->locale('ar')->dayName;

        //    dd($payment);
      /*  if ($day=='Sta') {
            $dayname = 'السبت';
        }else  if ($day=='Sun') {
            $dayname = 'الاحد';
        }else  if ($day=='Mon') {
            $dayname = 'الاثنين';
        }else  if ($day=='Tue') {
            $dayname = 'الثلاتاء';
        }else  if ($day=='Wen') {
            $dayname = 'الاربعاء';
        }else  if ($day=='Thr') {
            $dayname = 'الخميس';
        }else  if ($day=='Fri') {
            $dayname = 'الجمعة';
        }*/
        return view('admin.card_contracts.client_sanad',compact('total','one_pay','today_date','contract','date','payment','dayname','payment'));
    }



    public function qest_sadad(Request $request,$id){

        $payment=Payment::find($id);
        $qest_num=$request['key'];

        return view('admin.card_contracts.sadad',compact('payment','qest_num'));
    }
}
