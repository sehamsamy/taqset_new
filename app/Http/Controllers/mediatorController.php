<?php

namespace App\Http\Controllers;

use App\City;
use App\Mediator;
use App\User;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;

use App\Activity;

class  mediatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.mediators.index')->with('mediators',Mediator::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.mediators.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //dd($request->all());
        $this->validate($request,[
            'name'=>'required|string|',
            'national_id'=>'required|string|',


        ]);
        $inputs=$request->all();
       $mediator= Mediator::create($inputs);
        $text=' اضافه الوسيط '. $mediator->name;
      Activity($text);
        alert()->success('تم اضافة الوسيط بنجاح !')->autoclose(5000);
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $mediator=Mediator::find($id);
        return view('admin.mediators.edit',compact('mediator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mediator=Mediator::find($id);
        $this->validate($request,[
            'name'=>'required|string|',
            'national_id'=>'required|string|',


        ]);
        $inputs=$request->all();
        $mediator->update($inputs);
        $text=' تعديل الوسسيط '. $mediator->name;
        Activity($text);
        alert()->success('تم تعديل  الوسيط بنجاح !')->autoclose(5000);
        return redirect('dashboard/mediators');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { //dd($id);
        $mediator=Mediator::find($id);

            $mediator->delete();
        $text=' حذف الوسيط '. $mediator->name;
        Activity($text);
            alert()->success('تم حذف الوسيط  بنجاح');
            return back();

    }
}
