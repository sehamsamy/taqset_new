<?php

namespace App\Http\Controllers;

use App\Activity;
use App\City;
use App\Clearance;
use App\Client;
use App\Guarantor;
use App\User;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;

class clearanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.clearances.index')->with('clearances',Clearance::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients=Client::pluck('name','id')->toArray();
        return view('admin.clearances.add',compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request,[
            'name'=>'required|string|',

            'date'=>'required|string|',


        ]);
        $inputs=$request->all();

        $clearance= Clearance::create($inputs);
        $random = random_int(1, 9999);
        $clearance->update([
            'randam_num'=>$random
        ]);
        $text=' اضافه مخالصه '. $clearance->name;
        Activity($text);

        alert()->success('تم اضافة مخالصه بنجاح !')->autoclose(5000);
//        return back();
        return view('admin.clearances.sanad',compact('clearance','random'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $clearance=Clearance::find($id);
       // dd($clearance);

        return view('admin.clearances.sanad',compact('clearance','random'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $clearance=Clearance::find($id);

        $clients=Client::pluck('name','id')->toArray();
        return view('admin.clearances.edit',compact('clearance','clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $clearance=Clearance::find($id);
        $this->validate($request,[
            'name'=>'required|string|',
            'date'=>'required|string|',

        ]);
        $inputs=$request->all();
        $clearance->update($inputs);
        $text=' تعديل المخالصه '. $clearance->name;
        Activity($text);
        alert()->success('تم تعديل  المخالصه بنجاح !')->autoclose(5000);
return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { //dd($id);

        $clearance=Clearance::find($id);


        $clearance->delete();
        $text=' حذف المخالصه '. $clearance->name;
        Activity($text);
        alert()->success('تم حذف المخالصه  بنجاح');
        return back();

    }
}