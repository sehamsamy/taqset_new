<?php

namespace App\Http\Controllers;

use App\Activity;
use App\City;
use App\Client;
use App\Contract;
use App\Guarantor;
use App\User;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;

class clientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.clients.index')->with('clients',Client::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $guarantors=Guarantor::pluck('name','id')->toArray();
        return view('admin.clients.add',compact('guarantors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //dd($request->all());
        $this->validate($request,[
            'name'=>'required|string|',
            'national_id'=>'required|string|',
            'origin'=>'required|string|',


        ]);
        $inputs=$request->all();

       $client= Client::create($inputs);
        $text=' اضافه العميل '. $client->name;
        Activity($text);

        alert()->success('تم اضافة العميل بنجاح !')->autoclose(5000);
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $contracts=Contract::where('client_id',$id)->get();
        return view('admin.clients.view',compact('contracts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $client=Client::find($id);
        $guarantors=Guarantor::pluck('name','id')->toArray();
        return view('admin.clients.edit',compact('client','guarantors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $client=Client::find($id);
        $this->validate($request,[
            'name'=>'required|string|',
            'national_id'=>'required|string|',
            'origin'=>'required|string|',
        ]);
        $inputs=$request->all();
        $client->update($inputs);
        $text=' تعديل العميل '. $client->name;
        Activity($text);
        alert()->success('تم تعديل  العميل بنجاح !')->autoclose(5000);
        return redirect('dashboard/clients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { //dd($id);

        $client=Client::find($id);


            $client->delete();
        $text=' حذف العميل '. $client->name;
        Activity($text);
            alert()->success('تم حذف العميل  بنجاح');
            return back();

    }
}
