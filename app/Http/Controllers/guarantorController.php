<?php

namespace App\Http\Controllers;

use App\City;
use App\Guarantor;
use App\User;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;

class guarantorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.guarantors.index')->with('guarantors',Guarantor::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.guarantors.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //dd($request->all());
        $this->validate($request,[
            'name'=>'required|string|',
            'national_id'=>'required|string|',
            'origin'=>'required|string|',


        ]);
        $inputs=$request->all();

       $guarantor= Guarantor::create($inputs);
        $text=' اضافه كفيل '. $guarantor->name;
        Activity($text);

        alert()->success('تم اضافة الكفيل بنجاح !')->autoclose(5000);
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $guarantor=Guarantor::find($id);
        return view('admin.guarantors.edit',compact('guarantor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $guarantor=Guarantor::find($id);
        $this->validate($request,[
            'name'=>'required|string|',
            'national_id'=>'required|string|',
            'origin'=>'required|string|',

        ]);
        $inputs=$request->all();

        $guarantor->update($inputs);
        $text=' تعديل كفيل '. $guarantor->name;
        Activity($text);
        alert()->success('تم تعديل  الكفيل بنجاح !')->autoclose(5000);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { //dd($id);
        $guarantor=Guarantor::find($id);

        $guarantor->delete();
        $text=' حذف كفيل '. $guarantor->name;
        Activity($text);
            alert()->success('تم حذف الكفيل  بنجاح');
            return back();

    }
}
