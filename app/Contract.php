<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Attribute
 *
 * @property int $id
 * @property string $ar_name
 * @property string $en_name
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Attribute_value[] $attribute_values
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attribute newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attribute newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attribute query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attribute whereArName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attribute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attribute whereEnName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attribute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attribute whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attribute whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $branch_id
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Product[] $products
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attribute whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attribute whereDeletedAt($value)
 */
class Contract extends Model
{
    protected $fillable = ['clientv2_id','client_id', 'guarantor_id','mediator_id','type','kind','car_name','shas_num','car_num','color','model','number',
        'class','paid_amount','amount','added_value','notes','start_date','months_count'];
    public function guarantor()
    {
        return $this->belongsTo(Guarantor::class);
    }
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    public function mediator()
    {
        return $this->belongsTo(Mediator::class);
    }

}
