<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = [
        'text','user_id'
    ];
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}

