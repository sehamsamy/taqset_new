@extends('admin.layout.app')

@section('title')
    عرض  الاقساط
@endsection
@section('header')
    {{Html::style('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}
@endsection
@section('content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        عرض  الاقساط المتاخرة
                    </h2>


                </div>

                <div class="form-group form-float col-sm-6 col-xs-12">
                    <label >الاجمالى:
                        <label>
                            {{round($qests->sum('one_pay'),2)}}
                      </div>

                <div class="body">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable delet2">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th> اسم صاحب القسط</th>
                            <th> جوال صاحب القسط</th>
                            <th>تاريخ القسط</th>
                            <th>المبلغ المستحق </th>
                            <th>سداد </th>
                            <th> طباعه </th>

                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>تاريخ القسط</th>
                            <th>المبلغ المستحق </th>
                            <th>سداد </th>
                            <th> طباعه </th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($qests as  $key=>$qest)
                            <tr>

                                <td>{{++$key}}</td>
                                <td>{{$qest->contract->client->name}}</td>
                                <td>{{$qest->contract->client->phone}}</td>

                                <td>{{$qest->date}}</td>
                                <td>{{ floor($qest->one_pay)}}</td>

                        @if ($qest->pay==0)

                                    <td>
                                        {{--<a href="{{route('admin.card_contracts.sadad',['id'=>$qest->id])}}" class="btn btn-primary btn-circle"><i style="padding-top:5px;padding-left: 6px;" class="fa fa-money"></i></a>--}}

                                        <a href="#"  onclick="myFunction({{$qest->id}})"  data-toggle="tooltip" class="btn btn-primary btn-circle" ><i style="padding-top:5px;padding-left: 6px;" class="fa fa-money"></i></a>
                                        {!!Form::open( ['route' => ['admin.card_contracts.sadad',$qest->id] ,'id'=>'reason-form'.$qest->id, 'method' => 'get']) !!}
                                        <input type="hidden" name="key" value="{{$key}}">

                                        {!!Form::close() !!}
                                    </td>
                            @else
                            <td> تم السداد</td>

                                @endif

                                <td>


                                    {{--<a href="{{route('admin.card_contracts.sadad_qests',['id'=>$qest->id])}}" class="btn btn-info btn-circle"><i style="padding-top:5px;padding-left: 6px;" class="fa fa-print"></i></a>--}}

                                    {!!Form::open( ['route' => ['admin.card_contracts.sadad_qests',$qest->id] , 'method' => 'post']) !!}
                                    <input type="hidden" name="key" value="{{$key}}">

                                    <button class="btn btn-info " type="submit"><i style="padding-top:5px;padding-left: 6px;" class="fa fa-print"></i></button>
                                    {!!Form::close() !!}


                                    {!!Form::open( ['route' => ['admin.car_contracts.sadad',$qest->id] ,'id'=>'reason-form'.$qest->id, 'method' => 'get']) !!}
                                    <input type="hidden" name="" value="{{$qest->one_pay}}" id="one_pay">
                                    {!!Form::close() !!}

                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@section('footer')

    <script>
        function myFunction(id) {
            var item_id=id;
            var itemx= document.getElementById('one_pay');
            var value=  itemx.value;

            swal({


                title: "   سداد القسط",
                text: value +"هل  تريد سداد هذا القسط وقيمه المبلغ ؟",

                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function (isConfirm ) {
                if (isConfirm) {
                    console.log(item_id);
                    $('#ff').val($('#swal').val());
                    document.getElementById('reason-form'+item_id).submit();


                } else {
                    swal("تم االإلفاء", " سداد القسط  تم الغاؤه", 'info', {buttons: 'موافق'});
                }


            });


        }

    </script>
@endsection


@section('data-table')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>

    <script type="text/javascript"
            src="{{asset('admin/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('admin/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/forms/selects/select2.min.js')}}"></script>
<!--     <script type="text/javascript"
            src="{{asset('admin/js/plugins/datatables_extension_buttons_init.js')}}"></script> -->
        <script>
            $(function () {



            $.extend($.fn.dataTable.defaults, {
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '&larr;', 'previous': '&rarr;'}
                }
            });
            // Basic initialization
            $('table.js-exportable').DataTable({
                responsive: true,
                buttons: {
                    dom: {
                        button: {
                            className: 'btn btn-default'
                        }
                    },
                    buttons: [
                        {extend: 'copy'},
                        {extend: 'csv'},
                        {extend: 'excel'},
                        {extend: 'pdf'},
                        {extend: 'print' ,
                        title:"الاقساط المتأخرة",
                        customize: function ( win ) {
                                $(win.document.body)
                                    .css( 'font-size', '10pt' )
                                    .append(
                                        ` <div class="form-group divcustomesize">
                                <label >اجمالي مبالغ الاقساط:
                                    <label>
                                        {{round($qests->sum('one_pay'),2)}}
                                </div>`
                                    );


                            }
                    }
                    ]
                }
            });
            });
        </script>
@endsection
