
<?php
if ($contract->kind=="client")
{
    $id=$contract->clientv2_id;

    $secound=\App\Client::find($id);

}
else
{
    $id=$contract->guarantor_id;
    $secound=\App\Guarantor::find($id);

}



$months_count=$contract->months_count;
$start_date=$contract->start_date;

$end_date=\Carbon\Carbon::parse($start_date)->addMonths($months_count-1);



$end=$end_date->format('d/m/Y');

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>سند لأمر</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<style>
    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font: 12pt "Tahoma";
        direction: rtl;
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }

    /********* MODIFICATIONS ************/
    .content-top .left {
        text-align: center !important;
    }
    .spann {
        min-width: 50px !important;
    }
    span.dates.long {
        width: auto !important;
        display: inline-block !important;
        margin: 0 !important;
    }

    .date {
        min-width: 20px !important;
        width: auto !important;
    }
    /************************/
    .page {
        width: 210mm;
        min-height: 297mm;
        padding: 10mm 0mm;
        margin: 5mm;
        border: 10px #f00 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 0.5cm;
        border: 5px black solid;
        height: 282mm;
        outline: 0cm;
    }
    .subpage h2 {
        text-align: center;
        width: 100%;
        display: inline-block
    }
    /**  /////////////////////////////  REEM ////////////////////////////////////// */
    .head {
        border-bottom: 2px black solid;
        display: flex;
    }
    .logo {
        width: 80px;
        height: 80px;
        object-fit: contain;
    }
    .content-top {
        display: flex;
    }
    .date {
        font-weight: bold;
    }
    .right {
        width: 50%;
    }
    .p-right-width {
        width: 220px;
        line-height: 1.8;
        height: 60px;
        text-align: center;
    }
    .left {
        width: 50%;
    }
    .left {
        text-align: left;
    }
    .p-left {
        text-align: left;
    }
    .p-inline {
        display: inline-block;
    }
    .p-width {
        min-width: 50%;
        float: left;
    }
    .content {
        margin-top: 20px;
    }
    .date {
        width: 50px;
        display: inline-block;
        color: #b33133;
    }
    .text-center {
        text-align: center;
        margin: 0 auto;
    }
    .content h3{
        border-bottom: 1px solid;
        width: max-content;
    }
    p {
        line-height: 1.5;
    }
    .btm{
        width: 100%;
        min-height: 30px;
        display: block;
    }
    .bold {
        font-weight: bold;
    }
    .foot {
        margin-top: 30px;
        color: #b33133;
    }
    .spann {
        display: inline-block;
        min-width: 150px;
        text-align: center;
        color: #b33133;
    }
    .width-right {
        width: 62px !important;
        text-align: right;
        display: inline-block;
        font-weight: bold;
    }
    .margin {
        margin: 30px auto 10px auto;
    }
    .long{
        width: 100%;
        display: block;
        margin: 10px 0 0 0;
    }
    .sml{
        min-width: 50px;
    }
    /* Table */
    table,
    td,
    th {
        border: 1px solid black;
        text-align: center;
    }
    table {
        border-collapse: collapse;
        width: 100%;
        margin: 20px auto 0 auto;
    }

    th,
    td {
        height: 40px;
    }
    td{
        color: #b33133;
    }
    th {
        width: 160px;
    }
    .height-modifay{
        height: 90px;
    }
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html,
        body {
            width: 210mm;
            height: 297mm;
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
<body>
<div class="book">
    <div class="page">
        <div class="subpage">
            <div class="content">
                <h3 class="text-center bold">سند لأمر</h3>
                <div class="content-top">
                    <div class="right">
                        <p> <span class="bold">حرر بمدينة:-</span> <span> بريدة </span> </p>
                    </div>
                    <div class="left">
                        <span class="bold" >رقم السند:{{$contract->id}}</span><br>
                        <span class="bold"> بتاريخ :-</span>
                        <span class="dates">
                             {{$date}}
                         </span> هـ
                    </div>
                </div>
                <p>
                    <span class="bold"> الموافق :-</span>
                    <span class="dates">
                       {{$today_date}}

                        </span> م
                </p>
                <p>
                    <span class="bold">أتعهد أنا المدعو أدناه بأن أدفع بموجب هذا السند بدون قيد أو شرط لأمر / :-</span> <span class="btm">  عبد الرحمن رجاء العنزي </span>
                </p>
                <p>
                    مبلغا وقدره ( <span class="spann">{{($contract->class*$contract->number)+$contract->added_value}}</span> ) ريال <span class="spann"> {{$total}}</span>  فقط لا غير بتاريخ الاستحقاق
                    <span class="dates long">{{$end}}
                        </span>م
                </p>
                <p>
                    هذا السند واجب الدفع بدون تعلل بموجب قرار مجلس الوزراء الموقر رقم ( <span class="spann sml">692</span> ) وبتاريخ
                    <span class="dates">
                        <span class="date text-center"> 26</span>/
                        <span class="date text-center">09</span>/
                        <span class="date text-center">1383</span>
                        </span> هـ
                    والمتوج بالمرسوم الملكى الكريم رقم ( <span class="spann sml">37</span> ) بتاريخ
                    <span class="dates">
                        <span class="date text-center">11</span>/
                        <span class="date text-center">10</span>/
                        <span class="date text-center">1383</span>
                        </span> هـ
                    نظام الأوراق التجارية .
                </p>
                <p>
                    بموجب هذا السند يسقط المدين كافة حقوق التقديم والمطالبة والاحتجاج والاخطار بالامتناع عن الوفاء والمتعلقة بهذا السند .
                </p>
                <h3 class="text-center bold">محرر السند</h3>
                <table>

                    <tr>
                        <th>اسم الكفيل</th>
                        <td colspan="4"> {{$secound->name}}</td>
                        <th>رقم الهويه</th>
                        <td colspan="4">{{$secound->national_id}}</td>
                    </tr>

                    <tr>
                        <th>اسم المدين</th>
                        <td colspan="4"> {{$contract->client->name}}</td>
                        <th>رقم الهوية</th>
                        <td colspan="4">{{$contract->client->national_id}}</td>
                    </tr>
                    <tr>
                        <th>مكان الوفاء</th>
                        <td colspan="4">بريدة</td>
                        <th>العنوان</th>
                        <td colspan="4">{{$contract->client->address}}</td>
                    </tr>
                    <tr class="height-modifay">
                        <th>التوقيع</th>
                        <td colspan="4"></td>
                        <th>البصمة</th>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <th>رقم الجوال</th>
                        <td colspan="4">{{$contract->client->phone}}</td>
                        <th>رقم الهاتف</th>
                        <td colspan="4">{{$contract->client->phone}}</td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
    <script src="js/bootstrap.min.js"></script>

       <script type="text/javascript">
           window.print();
       </script>

</div>
</body>
</html>
