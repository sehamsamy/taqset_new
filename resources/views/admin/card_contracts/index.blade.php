@extends('admin.layout.app')

@section('title')
  عرض  بطاقات الشحن
@endsection
@section('header')
{{Html::style('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}
<style>
  th.sorting, tr > td {
    display: table-cell!important;
  }


</style>
@endsection
@section('content')

<!-- Exportable Table -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>
          عرض  بطاقات الشحن
        </h2>
        <ul class="header-dropdown m-r--5">
          <a href="{{route('admin.card_contracts.create')}}">   <button class="btn btn-success">إضافة بطاقات شحن جديد</button></a>
</ul>
        </div>
        <div class="body">
          <table class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
              <tr>
                <th>#</th>
                <th>العميل</th>
                <th>الوسيط</th>
                <th> الضامن </th>
                <th>الفئه </th>
                <th>العدد </th>
                <th> اجمالى المبلغ  تسديده </th>

                <th> تاريخ بدء الاقساط </th>
                <th> المتبقى </th>
                <th>الاقساط </th>
                <th>تقرير </th>
                <th>السندات </th>

                <th>العمليات</th>
              </tr>
            </thead>
       <!--   <tfoot>
            <tr>
              <th>#</th>
              <th>العميل</th>
              <th>الوسيط</th>

              <th> الضامن </th>
              <th>الفئه </th>
              <th>العدد </th>
              <th> اجمالى المبلغ  تسديده </th>
              <th> تاريخ بدء الاقساط </th>
              <th> المتبقى </th>
              <th>الاقساط </th>
              <th>تقرير </th>
              <th>السندات </th>

              <th>العمليات</th>
            </tr>
          </tfoot>-->
          <tbody>
            @foreach($card_contracts as $key=>$contract)
            <tr>
              <td> {{++$key}}</td>
              <td>{{@$contract->client->name}}</td>
              <td>{{@$contract->mediator->name}}</td>
                @if ($contract->kind=="client")
                  <?php
                  $id=$contract->clientv2_id;
                  $client=\App\Client::find($id);
                  ?>
                  <td>{{optional($client)->name}}</td>
                @else
                  <?php
                  $id=$contract->guarantor_id;
                  $guarantor=\App\Guarantor::find($id);
                  ?>
                  <td>{{optional($guarantor)->name}}</td>
                @endif

              <td>{{$contract->class}}</td>
              <td>{{$contract->number}}</td>
              <td>{{($contract->class*$contract->number)+$contract->added_value}}</td>
              <td>{{$contract->start_date}}</td>

              <td>
                <?php
                $not_paid=\App\Payment::where('contract_id',$contract->id)->where('pay',0)->sum('one_pay');
                ?>
                {{round($not_paid)}}

              </td>


              <td>
                <a href="{{route('admin.card_contracts.show',['id'=>$contract->id])}}" class="btn btn-primary btn-circle"><i style="padding-top:5px;padding-left: 3px;" class="fa fa-list"></i></a>
                </td>
              <td>
                <a href="{{route('admin.card_contracts.report',['id'=>$contract->id])}}" class="btn btn-primary btn-circle"><i style="padding-top:5px;padding-left: 3px;" class="fa fa-eye"></i></a>
              </td>
              <td>
                <a href="{{route('admin.card_contracts.client_sanad',['id'=>$contract->id])}}" class="btn btn-success ">سند العميل</a>
                <a href="{{route('admin.card_contracts.guarantor_sanad',['id'=>$contract->id])}}" class="btn btn-success ">سند الكفيل</a>

              </td>
                <td>
                  <a href="{{route('admin.card_contracts.print',['id'=>$contract->id])}}" class="btn btn-primary btn-circle"><i style="padding-top:5px;padding-left: 3px;" class="fa fa-print"></i></a>
                  @if (Auth::user()->edit_contract==1)

                  <a href="{{route('admin.card_contracts.edit',['id'=>$contract->id])}}" class="btn btn-info btn-circle"><i style="padding-top:5px;padding-left: 3px;" class="fa fa-pencil"></i></a>
                @endif
                    @if (Auth::user()->delete_contract==1)
                <a href="#"  onclick="Delete({{$contract->id}})"  data-original-title="حذف" class="btn btn-danger btn-circle"><i style="padding-top: 5px;padding-left: 3px;" class="fa fa-trash-o"></i></a>
                {!!Form::open( ['route' => ['admin.card_contracts.destroy',$contract->id] ,'id'=>'delete-form'.$contract->id, 'method' => 'Delete']) !!}
                {!!Form::close() !!}
                    @endif
              </td>

            </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- #END# Exportable Table -->

@endsection

@section('footer')
  <script>
    function Delete(id) {
      var item_id=id;
      console.log(item_id);
      swal({
        title: "هل أنت متأكد ",
        text: "هل تريد حذف هذا العقد ؟",
        icon: "warning",
        buttons: ["الغاء", "موافق"],
        dangerMode: true,

      }).then(function(isConfirm){
        if(isConfirm){
          document.getElementById('delete-form'+item_id).submit();
        }
        else{
          swal("تم االإلفاء", "حذف  المستخدم تم الغاؤه",'info',{buttons:'موافق'});
        }
      });
    }
  </script>

  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/jszip.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js')!!}
@endsection


@section('data-table')
   <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>

    <script type="text/javascript"
            src="{{asset('admin/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('admin/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('admin/js/plugins/datatables_extension_buttons_init.js')}}"></script>
@endsection
