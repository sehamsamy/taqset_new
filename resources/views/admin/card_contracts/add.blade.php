@extends('admin.layout.app')

@section('title')
إضافة  عقد بطاقات شحن
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>إضافة  عقد بطاقات شحن</h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{route('admin.card_contracts.index')}}">    <button class="btn btn-danger">كل عقود البطاقات </button></a>
        </ul>
      </div>
      <div class="body">
          {!!Form::open( ['route' => 'admin.card_contracts.store' ,'class'=>'form phone_validate', 'method' => 'Post','files' => true]) !!}
        @include('admin.card_contracts.form')
        {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>
<!-- #END# Basic Validation -->
@endsection


