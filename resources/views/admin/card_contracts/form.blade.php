@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="col-sm-6 col-xs-12 pull-right">
    <div class="form-group form-float ">
        <label class="form-label" >الوسيط -الطرف الاول </label>

        {!! Form::select("mediator_id",$mediators,null,['class'=>'form-control js-example-basic-single','placeholder'=>'اختر الوسيط   '])!!}
    </div>
</div>


<div class="col-sm-6 col-xs-12 pull-right ">
    <div class="form-group form-float ">
        <label class="form-label" >العميل- الطرف الثانى </label>
        {!! Form::select("client_id",$clients,null,['class'=>'form-control js-example-basic-single','placeholder'=>'اختر العميل   '])!!}
    </div>
</div>


@if (!isset($contract))
    <div class="col-xs-6 pull-right">

        <div class="form-group form-float ">
            <label> الضامن</label>
            <div class="form-line">

                <input  type="radio" value="client" name="kind"  id="radio_5"  onclick="myFunction()" checked />

                <label for="radio_5">عميل</label>

                <input  type="radio" value="guarantor" name="kind" id="radio_4"  onclick="myFunction3()">
                <label for="radio_4"> كفيل </label>

            </div>
        </div>
    </div>
@endif

@if (isset($contract))
    @if ($contract->kind=="client")
        <div class="col-xs-6 pull-right">

            <div class="form-group form-float ">
                <label> الضامن</label>
                <div class="form-line">
                    <input  type="radio" value="client" name="kind"  id="radio_5"  onclick="myFunction()" checked />
                    <label for="radio_5">عميل</label>
                    <input  type="radio" value="guarantor" name="kind" id="radio_4"  onclick="myFunction3()">
                    <label for="radio_4"> كفيل </label>

                </div>
            </div>
        </div>
    @elseif($contract->kind=="guarantor")

        <div class="col-xs-6 pull-right">

            <div class="form-group form-float ">
                <label> الضامن</label>
                <div class="form-line">

                    <input  type="radio" value="client" name="kind"  id="radio_5"  onclick="myFunction()"  />

                    <label for="radio_5">عميل</label>

                    <input  type="radio" value="guarantor" name="kind" id="radio_4"  onclick="myFunction3()" checked>
                    <label for="radio_4"> كفيل </label>

                </div>
            </div>
        </div>
    @endif
@endif

<div class="col-sm-6 col-xs-12 pull-right clients">
<div class="form-group form-float ">
    <label class="form-label" >الضامن -عميل </label>
    {!! Form::select("clientv2_id",$clients,null,['class'=>'form-control js-example-basic-single','placeholder'=>'اختر العميل   '])!!}
</div>
</div>

<div class="col-sm-6 col-xs-12 pull-right guarantors">
    <div class="form-group form-float ">
        <label class="form-label" > الضامن -كفيل </label>
        {!! Form::select("guarantor_id",$guarantors,null,['class'=>'form-control js-example-basic-single','placeholder'=>'اختر الكفيل   '])!!}
    </div>
</div>




<div class="col-sm-6 col-xs-12 pull-right">
    <div class="form-group form-float ">
        <label class="form-label">فئة</label>
        <div class="form-line">
            {!! Form::text("class",null,['class'=>'form-control','placeholder'=>'  فئة  '])!!}
        </div>
    </div>
</div>



<div class="col-sm-6 col-xs-12 pull-right">
    <div class="form-group form-float ">
        <label class="form-label">عدد</label>
        <div class="form-line">
            {!! Form::number("number",null,['class'=>'form-control','placeholder'=>'  الاسم  '])!!}
        </div>
    </div>
</div>


{{--<div class="col-sm-6 col-xs-12">--}}
    {{--<div class="form-group form-float">--}}
        {{--<label class="form-label">مبلغ القرض</label>--}}
        {{--<div class="form-line">--}}
            {{--{!! Form::text("amount",null,['class'=>'form-control','placeholder'=>'  مبلغ القرض  '])!!}--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}




{{--<div class="col-sm-6 col-xs-12">--}}
    {{--<div class="form-group form-float">--}}
        {{--<label class="form-label">المبغ المدفوع</label>--}}
        {{--<div class="form-line">--}}
            {{--{!! Form::text("paid_amount",null,['class'=>'form-control','placeholder'=>'  المبلغ المدفوع  '])!!}--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="col-sm-6 col-xs-12 pull-right">
    <div class="form-group form-float ">
        <label class="form-label"> القيمه المضافه</label>
        <div class="form-line">
            {!! Form::text("added_value",null,['class'=>'form-control','placeholder'=>'  القيمه المضافه  '])!!}
        </div>
    </div>
</div>



<div class="col-sm-6 col-xs-12 pull-right">
    <div class="form-group form-float ">
        <label>بدايه شهور السداد</label>
        {!! Form::date("start_date",null,['class'=>'form-control'])!!}
        <div class="form-line">
        </div>
    </div>
</div>

<div class="col-sm-6 col-xs-12 pull-right">
    <div class="form-group form-float ">
        <label class="form-label">عدد شهور السداد</label>
        <div class="form-line">
            {!! Form::number("months_count",null,['class'=>'form-control','placeholder'=>'  عدد شهور السداد  '])!!}
        </div>
    </div>
</div>

<div class="col-sm-6 col-xs-12 pull-right">
    <div class="form-group form-float ">
        <label class="form-label">  ملاحظات</label>
        <div class="form-line">
            {!! Form::text("notes",null,['class'=>'form-control','placeholder'=>'   ملاحظات  '])!!}
        </div>
    </div>
</div>


<div class="col-xs-12">
    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>
</div>

@section('footer')
    <?php
    if (!isset($contract))

    {
    ?>
    <script>

        $(document).ready(function() {
            $(".guarantors").hide();

        });

    function myFunction() {
        $(".clients").show();
        $(".guarantors").hide();
    }
        function myFunction3() {
            $(".clients").hide();
            $(".guarantors").show();
        }


        </script>

    <?php
            }
    ?>
    <?php
    if (isset($contract))
        if($contract->kind=="guarantor")
            {
            ?>
            <script>
            $(".clients").hide();
            $(".guarantors").show();

            function myFunction() {
                $(".clients").show();
                $(".guarantors").hide();
            }
            function myFunction3() {
                $(".clients").hide();
                $(".guarantors").show();
            }

            </script>
            <?php
       }else{
            ?>

    <script>
        $(".guarantors").hide();
        $(".clients").show();
        function myFunction() {
            $(".clients").show();
            $(".guarantors").hide();
        }
        function myFunction3() {
            $(".clients").hide();
            $(".guarantors").show();
        }

    </script>
    <?php
    }
    ?>
@endsection