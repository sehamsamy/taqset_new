@extends('admin.layout.app')
@section('title')
تعديل العقد
{{ $contract->id }}
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>تعديل العقد      {{ $contract->id }}</h2>
        <ul class="header-dropdown m-r--5">
          <a href="{{route('admin.card_contracts.index')}}">    <button class="btn btn-danger">كل عقود بطاقات الشحن </button></a>
        </ul>
      </div>
      <div class="body">
        {!!Form::model($contract , ['route' => ['admin.card_contracts.update' , $contract->id] , 'method' => 'PATCH']) !!}
        @include('admin.card_contracts.form')
        {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>


<!-- #END# Basic Validation -->
@endsection

@if(isset($guarantor))


@section('footer')
  <script>
    // Initialize and add the map
    function initMap() {
      // The location of Uluru
      var uluru = {lat: {{$user->lat}}, lng: {{$user->long}}};
      // The map, centered at Uluru
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: uluru
      });
      // The marker, positioned at Uluru
      var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        draggable:true,
      });


      marker.addListener('drag', handleEvent);
      marker.addListener('dragend', handleEvent);
      document.getElementById('lat').value = {{$user->lat}};
      document.getElementById('lng').value = {{$user->long}};
    }

    function handleEvent(event) {
      document.getElementById('lat').value = event.latLng.lat();
      document.getElementById('lng').value = event.latLng.lng();
    }
  </script>


  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsT140mx0UuES7ZwcfY28HuTUrTnDhxww&callback=initMap">
  </script>
@endsection


@endif



