@extends('admin.layout.app')

@section('title')
 تقرير
@endsection
@section('header')
    <style type="text/css">
        .card .header h2{
            display: inline-block;
        }
        #btn{
            margin: 0 50px 0 0;
        }

        @page {
            size: A4;
            margin: 0;
        }
        @media print {
            html,
            body {
                width: 210mm;
                height: 297mm;
            }
            #DivIdToPrint  {
                margin: 0;
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
        }

        @media print
        {
            body * { visibility: hidden; }
            #DivIdToPrint * { visibility: visible; }
            #DivIdToPrint {
                position: absolute;
                top: 20px;
                left: 0;
                right: 0;
                text-align: right !important;
                padding: 0;
                margin: 0 auto;
                width: 100%;

            }
            section.content{
                margin: 0 !important;
            }
        }

    </style>
    {{Html::style('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}
@endsection
@section('content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                         عرض  الاقساط للعقد {{$contract->id}}
                    </h2>

                    <button id="btn" onclick="window.print()">طباعة التقرير</button>

                </div>
                <div class="body" id='DivIdToPrint'>



                    <div class="clearfix"></div>

                    <div class="col-md-3 pull-right">
                        <label> اسم العميل : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default">


                                {{$contract->client->name}}

                        </label>
                    </div>


                    <div class="col-md-3 pull-right">
                        <label> اسم الضامن : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default">


                            @if ($contract->kind=="client")
                                <?php
                                $id=$contract->clientv2_id;
                                $client=\App\Client::find($id);
                                ?>
                                {{$client->name}}
                               @else
                                <?php
                                $id=$contract->guarantor_id;
                                $guarantor=\App\Guarantor::find($id);
                                ?>
                               {{$guarantor->name}}
                            @endif
                        </label>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-3 pull-right">
                        <label> فئه البطاقات: </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{$contract->class}}</label>
                    </div>




                    <div class="col-md-3 pull-right">
                        <label> عدد البطاقات : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{$contract->number}}</label>
                    </div>



                    <div class="clearfix"></div>






                    <div class="col-md-3 pull-right">
                        <label>  القيمه المضافه : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{$contract->added_value}}</label>
                    </div>


                    <div class="clearfix"></div>



                    <div class="col-md-3 pull-right">
                        <label>   المبلغ المستحق : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{($contract->class*$contract->number)+$contract->added_value}}</label>
                    </div>





                    <div class="clearfix"></div>

                    <div class="col-md-3 pull-right">
                        <label>   عدد الاقساط المدفوعه : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default">{{$paymemts_paid}}</label>
                    </div>


                    <div class="col-md-3 pull-right">
                        <label>   عدد الاقساط المتبقيه : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default">{{$paymemts_notpaid}}</label>
                    </div>


                    <div class="clearfix"></div>

                    <div class="col-md-3 pull-right">
                        <label>   ماتم تسديده : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{$paid}}</label>
                    </div>




                    <div class="col-md-3 pull-right">
                        <label>  متبقى : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{$not_paid}}</label>
                    </div>


                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>

                    <center>
                        <div class="box-header">

                            <h3 class="box-title">الاقساط المتبقية</h3>

                        </div>
                    </center>
                    <table  class="table table-bordered table-striped data-table">

                        <thead>

                        <tr>

                            <th>#</th>

                            <th>تاريخ القسط</th>

                            <th>قيمه القسط</th>

                        </tr>

                        </thead>

                        <tbody>

              @foreach($payments as $key=>$payment)
                  <tr>
                      <td>{{$key+1}} </td>
                  <td>{{$payment->date}} </td>
                  <td>{{ceil($payment->one_pay)}} </td>
                  </tr>
                  @endforeach
                        </tbody>

                        <tfoot>

                        <tr>

                            <th>#</th>


                            <th>تاريخ القسط</th>
                            <th>قيمه القسط</th>


                        </tr>

                        </tfoot>

                    </table>



                </div>

                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@section('footer')
    <script>
        function Delete(id) {
            var item_id=id;
            console.log(item_id);
            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذا المستخدم ؟",
                icon: "warning",
                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('delete-form'+item_id).submit();
                }
                else{
                    swal("تم االإلفاء", "حذف  المستخدم تم الغاؤه",'info',{buttons:'موافق'});
                }
            });
        }
    </script>



@endsection


@section('data-table')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>

    <script type="text/javascript"
            src="{{asset('admin/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('admin/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('admin/js/plugins/datatables_extension_buttons_init.js')}}"></script>
@endsection
