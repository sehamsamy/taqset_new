@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<input type="hidden" name="id" value="@isset($user) {!! $user->id !!} @endisset">
<div class="col-sm-12 col-xs-12  pull-right">
    <div class="form-group form-float">
  <label class="form-label">الإسم</label>
  <div class="form-line">
    {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'  الاسم  '])!!}
  </div>
</div>
</div>

<div class="col-sm-12 col-xs-12  pull-right">
<div class="form-group form-float">
  <label class="form-label">الإيميل</label>
  <div class="form-line">
        {!! Form::email("email",null,['class'=>'form-control','placeholder'=>'  الايميل '])!!}

  </div>
</div>
</div>



<div class="col-sm-12 col-xs-12  pull-right">
<div class="form-group form-float">
  <label class="form-label">الباسورد</label>
    <input type="password" class="form-control" name="password" >
    <div class="form-line">
  </div>
</div>
</div>

<div class="col-sm-12 col-xs-12  pull-right">
<div class="form-group form-float">
  <label class="form-label">تكرار الباسورد</label>
    <input type="password" class="form-control" name="password_confirmation" >
    <div class="form-line">
  </div>
</div>
</div>





<div class="col-xs-12  pull-right">
    <div class="form-group form-float"><!-- form-field -->
        <label>الصلاحيه</label>

        {!! Form::select("is_admin",['1'=>'ادمن','0'=>'مستخدم'],null,['class'=>'form-control'])!!}

    </div>
</div>

@if (isset($user))
    @if ($user->delete_client==1)
        <div class="col-xs-6  pull-right">
            <div class="form-group form-float">
                <label>حذف العميل</label>
                <div class="form-line">

                    <input  type="radio" value="1" name="delete_client"  id="radio_1"  checked />

                    <label for="radio_1">نعم</label>

                    <input  type="radio" value="0" name="delete_client" id="radio_3" >
                    <label for="radio_3"> لا </label>

                </div>
            </div>
        </div>
        @else
        <div class="col-xs-6  pull-right">
            <div class="form-group form-float">
                <label>حذف العميل</label>
                <div class="form-line">

                    <input  type="radio" value="0" name="delete_client"  id="radio_1"  checked />

                    <label for="radio_1">نعم</label>

                    <input  type="radio" value="1" name="delete_client" id="radio_3"  checked >
                    <label for="radio_3"> لا </label>

                </div>
            </div>
        </div>
    @endif

  @else
    <div class="col-xs-6  pull-right">
        <div class="form-group form-float">
            <label>حذف العميل</label>
            <div class="form-line">

                <input  type="radio" value="1" name="delete_client"  id="radio_1"  checked />

                <label for="radio_1">نعم</label>

                <input  type="radio" value="0" name="delete_client" id="radio_3" >
                <label for="radio_3"> لا </label>

            </div>
        </div>
    </div>

@endif



@if (isset($user))
    @if ($user->delete_contract==1)
<div class="col-xs-6  pull-right">

<div class="form-group form-float">
    <label>حذف العقد</label>
    <div class="form-line">

        <input  type="radio" value="1" name="delete_contract"  id="radio_5"  checked />

        <label for="radio_5">نعم</label>

        <input  type="radio" value="0" name="delete_contract" id="radio_4" >
        <label for="radio_4"> لا </label>

    </div>
</div>
</div>
@else
        <div class="col-xs-6  pull-right">

            <div class="form-group form-float">
                <label>حذف العقد</label>
                <div class="form-line">

                    <input  type="radio" value="0" name="delete_contract"  id="radio_5"   />

                    <label for="radio_5">نعم</label>

                    <input  type="radio" value="1" name="delete_contract" id="radio_4" checked>
                    <label for="radio_4"> لا </label>

                </div>
            </div>
        </div>
   @endif

    @else
    <div class="col-xs-6  pull-right">

        <div class="form-group form-float">
            <label>حذف العقد</label>
            <div class="form-line">

                <input  type="radio" value="1" name="delete_contract"  id="radio_5"  checked />

                <label for="radio_5">نعم</label>

                <input  type="radio" value="0" name="delete_contract" id="radio_4" >
                <label for="radio_4"> لا </label>

            </div>
        </div>
    </div>



@endif













@if (isset($user))
    @if ($user->edit_contract==1)
        <div class="col-xs-6  pull-right">

            <div class="form-group form-float">
                <label>تعديل العقد</label>
                <div class="form-line">

                    <input  type="radio" value="1" name="edit_contract"  id="radio_5"  checked />

                    <label for="radio_5">نعم</label>

                    <input  type="radio" value="0" name="edit_contract" id="radio_4" >
                    <label for="radio_4"> لا </label>

                </div>
            </div>
        </div>
    @else
        <div class="col-xs-6  pull-right">

            <div class="form-group form-float">
                <label>تعديل العقد</label>
                <div class="form-line">

                    <input  type="radio" value="0" name="edit_contract"  id="radio_5"   />

                    <label for="radio_5">نعم</label>

                    <input  type="radio" value="1" name="edit_contract" id="radio_4" checked>
                    <label for="radio_4"> لا </label>

                </div>
            </div>
        </div>
    @endif

@else
    <div class="col-xs-6  pull-right">

        <div class="form-group form-float">
            <label>تعديل العقد</label>
            <div class="form-line">

                <input  type="radio" value="1" name="edit_contract"  id="radio_5"  checked />

                <label for="radio_5">نعم</label>

                <input  type="radio" value="0" name="edit_contract" id="radio_4" >
                <label for="radio_4"> لا </label>

            </div>
        </div>
    </div>



@endif


<div class="col-xs-12">
    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>
</div>

