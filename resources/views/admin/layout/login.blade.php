<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>تسجيل الدخول - برنامج التقسيط</title>
  <!-- Favicon-->
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Changa" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

  <!-- Bootstrap Core Css -->
  {!!Html::style('admin/plugins/bootstrap/css/bootstrap.css')!!}

  <!-- Waves Effect Css -->
  {!!Html::style('admin/plugins/node-waves/waves.css')!!}

  <!-- Animation Css -->
  {!!Html::style('admin/plugins/animate-css/animate.css')!!}

  <!-- Custom Css -->
  {!!Html::style('admin/css/style.css')!!}

  <style>
*
{
    font-family: 'Changa', sans-serif;
}
  </style>

</head>

<body class="login-page" dir="rtl">
  <div class="flex-img">
     <img src="{{asset('admin/images/login.svg')}}" alt="">
      <div class="login-box">
    <div class="card">
      <div class="body">

        <form id="sign_in" role="form" method="POST" action="{{route('login')}}">
          @if (count($errors) > 0)
          <div class="alert alert-info">
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

          {{ csrf_field() }}
            
            <div class="logo">
                <img src="{{asset('admin/images/logo.jpg')}}">
            </div>
          <div class="msg">تسجيل الدخول</div>
          <div class="input-group">
            <span class="input-group-addon">
              <i class="material-icons">email</i>
            </span>
            <div class="form-line">
              <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"  name="email" value="{{ old('email') }}"  placeholder="Email" required autofocus>
            </div>
          </div>
          <div class="input-group">
            <span class="input-group-addon">
              <i class="material-icons">lock</i>
            </span>
            <div class="form-line">
              <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
            </div>

          </div>
          <div class="row">

          
              <button class="btn btn-block bg-blue waves-effect" type="submit">دخول</button>
          
          </div>

        </form>
      </div>
    </div>
  </div>
  </div>

  <!-- Jquery Core Js -->
  {!!Html::script('admin/plugins/jquery/jquery.min.js')!!}

  <!-- Bootstrap Core Js -->
  {!!Html::script('admin/plugins/bootstrap/js/bootstrap.js')!!}

  <!-- Waves Effect Plugin Js -->
  {!!Html::script('admin/plugins/node-waves/waves.js')!!}

  <!-- Validation Plugin Js -->
  {!!Html::script('admin/plugins/jquery-validation/jquery.validate.js')!!}

  <!-- Custom Js -->
  {!!Html::script('admin/js/admin.js')!!}
  {!!Html::script('admin/js/pages/examples/sign-in.js"')!!}
</body>

</html>
