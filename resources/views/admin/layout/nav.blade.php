<li>
  <a class="check_active" href="{{route('admin.layout.main')}}">
    <i class="material-icons">home</i>
    <span>الصفحه الرئيسه والاحصائيات</span>
  </a>

</li>
@if ( auth()->user()->is_admin == 1)


  <li>
    <a href="javascript:void(0);" class="menu-toggle">
      <i class="material-icons">assignment_ind</i>
      <span>المستخدمين</span>
    </a>
    <ul class="ml-menu">
      <li >
        <a class="check_active" href="{{route('admin.users.index')}}">

          <span> عرض المستخدمين</span>
        </a>
      </li>
      <li >
        <a  class="check_active" href="{{route('admin.users.create')}}">

          <span>اضافه مستخدم جديد</span>
        </a>
      </li>
    </ul>
  </li>

@endif

  <li>
    <a href="javascript:void(0);" class="menu-toggle">
      <i class="material-icons">assignment_ind</i>
      <span>العملاء</span>
    </a>
    <ul class="ml-menu">
      <li >
        <a class="check_active" href="{{route('admin.clients.index')}}">

          <span> عرض العملاء</span>
        </a>
      </li>
      <li >
        <a  class="check_active" href="{{route('admin.clients.create')}}">

          <span>اضافه عميل جديد</span>
        </a>
      </li>
    </ul>
  </li>


  <li>
    <a href="javascript:void(0);" class="menu-toggle">
      <i class="material-icons">assignment_ind</i>
      <span>الكفلاء</span>
    </a>
    <ul class="ml-menu">
      <li >
        <a class="check_active" href="{{route('admin.guarantors.index')}}">

          <span> عرض الكفلاء</span>
        </a>
      </li>
      <li >
        <a  class="check_active" href="{{route('admin.guarantors.create')}}">

          <span>اضافه كفيل جديد</span>
        </a>
      </li>
    </ul>
  </li>

  <li>
    <a href="javascript:void(0);" class="menu-toggle">
      <i class="material-icons">assignment_ind</i>
      <span>الوسطاء-الطرف الاول</span>
    </a>
    <ul class="ml-menu">
      <li >
        <a class="check_active" href="{{route('admin.mediators.index')}}">

          <span> عرض الوسطاء</span>
        </a>
      </li>
      <li >
        <a  class="check_active" href="{{route('admin.mediators.create')}}">

          <span>اضافه وسيط جديد</span>
        </a>
      </li>
    </ul>
  </li>


  <li>
    <a href="javascript:void(0);" class="menu-toggle">
      <i class="material-icons">assignment_ind</i>
      <span>المخالصات</span>
    </a>
    <ul class="ml-menu">
      <li >
        <a class="check_active" href="{{route('admin.clearances.index')}}">

          <span> عرض المخالصات</span>
        </a>
      </li>
      <li >
        <a  class="check_active" href="{{route('admin.clearances.create')}}">

          <span>اضافه مخالصه  جديد</span>
        </a>
      </li>
    </ul>
  </li>


  <li>
    <a href="javascript:void(0);" class="menu-toggle">
      <i class="material-icons">assignment_ind</i>
      <span>العقود</span>
    </a>
    <ul class="ml-menu">
      <li >
        <a class="check_active" href="{{route('admin.car_contracts.index')}}">

          <span> عقود السيارات</span>
        </a>
      </li>
      <li >
        <a  class="check_active" href="{{route('admin.card_contracts.index')}}">

          <span>عقود بطاقات الشحن</span>
        </a>
      </li>
    </ul>
  </li>



@if ( auth()->user()->is_admin == 1)
<li>
  <a href="javascript:void(0);" class="menu-toggle">
    <i class="material-icons">assignment_ind</i>
    <span>الاعدادات</span>
  </a>
  <ul class="ml-menu">
    <li >
      <a class="check_active" href="{{route('admin.settings.edit',1)}}">

        <span> عرض الاعدادت</span>
      </a>
    </li>

  </ul>
</li>
  @endif

  @if ( auth()->user()->is_admin == 1)
<li>
  <a href="javascript:void(0);" class="menu-toggle">
    <i class="material-icons">assignment_ind</i>
    <span>التقارير</span>
  </a>
  <ul class="ml-menu">
    <li >
      <a class="check_active" href="{{route('admin.reports.collected_premiums')}}">

        <span>  تقرير الاقساط المحصلة</span>
      </a>
    </li>

  </ul>
</li>
  @endif
