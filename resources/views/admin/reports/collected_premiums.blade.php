@extends('admin.layout.app')

@section('title')
تقرير الاقساط المحصله خلال مده
@endsection
@section('header')
  {{Html::style('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}

@endsection
@section('content')
  <!-- Exportable Table -->
  <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>
            تقرير الاقساط المحصله خلال مده
          </h2>

        </div>
        <div class="body">
            <section class="filter">
                <div class="yurSections">
                    <div class="row">
                        <div class="col-xs-12">
                            <form action="" method="post" accept-charset="utf-8">
                                @csrf


                                <div class="col-sm-6 col-xs-12  pull-right">
                                    <div class="form-group form-float">
                                    <label for="from"> الفترة من </label>
                                    {!! Form::date("from",null,['class'=>'form-control datepicker'])!!}
                                </div>
                                </div>

                                <div class="col-sm-6 col-xs-12  pull-right">
                                    <div class="form-group form-float">
                                      <label for="to"> الفترة إلي </label>
                                        {!! Form::date("to",null,['class'=>'form-control datepicker'])!!}
                                    </div>
                                </div>


                            <div class="form-group col-xs-12">
                                <button type="submit" class="btn btn-success btn-block">بحث</button>
                            </div>
                            </form>


                        </div>
                        <div class="col-sm-6 col-xs-12  pull-right">
                            <div class="form-group form-float">
                                        <label >الاجمالى:
                                            <label>
                                                {{$premiums->sum('amount')}}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
          <table class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
            <tr>
              <th>#</th>
              <th>اسم صاحب السقط</th>
              <th> الجوال </th>
              <th>  المبلغ </th>

            </tr>
            </thead>

            <tbody>
            @foreach($premiums as $key=>$premium)
              <tr>
              <td>{{++$key}}</td>
              <td>{{$premium->payment->contract->client->name}}</td>
              <td>{{$premium->payment->contract->client->phone}}</td>
              <td>{{$premium->amount}}</td>

              </tr>
            @endforeach
            </tbody>
            <tfoot>
                <tr>
                  <th>#</th>
                  <th>اسم صاحب السقط</th>
                  <th> الجوال </th>
                  <th>  المبلغ </th>
                </tr>
                </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- #END# Exportable Table -->

@endsection

@section('footer')
  <script>
    function Delete(id) {
      var item_id=id;
      console.log(item_id);
      swal({
        title: "هل أنت متأكد ",
        text: "هل تريد حذف هذا العقد ؟",
        icon: "warning",
        buttons: ["الغاء", "موافق"],
        dangerMode: true,

      }).then(function(isConfirm){
        if(isConfirm){
          document.getElementById('delete-form'+item_id).submit();
        }
        else{
          swal("تم االإلفاء", "حذف  المستخدم تم الغاؤه",'info',{buttons:'موافق'});
        }
      });
    }
  </script>


{!!Html::script('admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')!!}
{!!Html::script('admin/plugins/jquery-datatable/extensions/export/jszip.min.js')!!}
{!!Html::script('admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js')!!}
{!!Html::script('admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js')!!}
{!!Html::script('admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')!!}
{!!Html::script('admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js')!!}

<script type="text/javascript" src="{{asset('/admin/hijri-calender/js/jquery.calendars.js')}}"></script>

<script type="text/javascript" src="{{asset('/admin/hijri-calender/js/jquery.calendars.plus.js')}}"></script>
<script type="text/javascript" src="{{asset('/admin/hijri-calender/js/jquery.plugin.js')}}"></script>
<script type="text/javascript" src="{{asset('/admin/hijri-calender/js/jquery.calendars.picker.js')}}"></script>

<script type="text/javascript" src="{{asset('/admin/hijri-calender/js/jquery.calendars.ummalqura.js')}}"></script>
<script>
    (function ($) {
        $.calendars.calendars.ummalqura.prototype.regionalOptions['ar'] = {
            name: 'Islamic',
            epochs: ['BAM', 'AM'],
            monthNames: ['محرّم', 'صفر', 'ربيع الأول', 'ربيع الآخر أو ربيع الثاني', 'جمادى الاول', 'جمادى الآخر أو جمادى الثاني',
                'رجب', 'شعبان', 'رمضان', 'شوّال', 'ذو القعدة', 'ذو الحجة'],
            monthNamesShort: ['محرّم', 'صفر', 'ربيع الأول', 'ربيع الآخر أو ربيع الثاني', 'جمادى الاول', 'جمادى الآخر أو جمادى الثاني',
                'رجب', 'شعبان', 'رمضان', 'شوّال', 'ذو القعدة', 'ذو الحجة'],
            dayNames: ['اح', 'اث', 'ث', 'ار', 'خ', 'ج', 'س'],
            dayNamesShort: ['اح', 'اث', 'ث', 'ار', 'خ', 'ج', 'س'],
            dayNamesMin: ['اح', 'اث', 'ث', 'ار', 'خ', 'ج', 'س'],
            digits: $.calendars.substituteDigits(['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩']),
            dateFormat: 'yyyy-mm-dd',
            firstDay: 6,
            isRTL: true
        };
    })(jQuery);
    $(function () {

        var calendar = $.calendars.instance('ummalqura', 'ar');
        $('.datepicker-hijri_1').calendarsPicker({
            calendar: calendar,
            defaultDate: '-1d',

            onSelect: function (date) {
                var selectHijriDate = date[0];
                //  convertToGeorgian(selectHijriDate.toString(),"national_id_date"); // ajax request

            }
        });


    });
</script>
@endsection

@section('data-table')

  <script type="text/javascript"
          src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
  <script type="text/javascript"
          src="{{asset('admin/js/plugins/tables/datatables/datatables.min.js')}}"></script>
  <script type="text/javascript"
          src="{{asset('admin/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
  <script type="text/javascript"
          src="{{asset('admin/js/plugins/forms/selects/select2.min.js')}}"></script>
  <!-- <script type="text/javascript"
          src="{{asset('admin/js/plugins/datatables_extension_buttons_init.js')}}"></script> -->
          <script>
            $(function () {



            $.extend($.fn.dataTable.defaults, {
                autoWidth: false,
                dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '&larr;', 'previous': '&rarr;'}
                }
            });
            // Basic initialization
            $('table.js-exportable').DataTable({
                responsive: true,
                buttons: {
                    dom: {
                        button: {
                            className: 'btn btn-default'
                        }
                    },
                    buttons: [
                        {extend: 'copy'},
                        {extend: 'csv'},
                        {extend: 'excel'},
                        {extend: 'pdf'},
                        {extend: 'print' ,
                        title:"الاقساط المتأخرة",
                        customize: function ( win ) {
                                $(win.document.body)
                                    .css( 'font-size', '10pt' )
                                    .append(
                                        `<div class="aligneRIght">
                                           <div>المدة الزمنية التي تمت الفترة بها هي

                                           <span>15 / 3 / 2020 </span> الي <span> 1 / 10 / 2020</span>
                                           </div>
                                           <div class="mt-5">
                                           تاريخ القسط
                                           <span>1 /9 /2020 </span>
                                           </div>
                                           <div class="mt-5">
                                           تاريخ القيام بعملية السداد
                                           <span>1 /9 /2020 </span>
                                           </div>
                                            <div class="form-group divcustomesize">
                                            <label >اجمالي مبالغ الاقساط:
                                                <label>
                                                {{$premiums->sum('amount')}}
                                            </div>
                                        </div>`
                                    );


                            }
                    }
                    ]
                }
            });
            });
        </script>
@endsection
