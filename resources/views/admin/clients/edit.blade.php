@extends('admin.layout.app')
@section('title')
تعديل العميل
{{ $client->name }}
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>تعديل العميل      {{ $client->name }}</h2>
        <ul class="header-dropdown m-r--5">
          <a href="{{route('admin.clients.index')}}">    <button class="btn btn-danger">كل العملاء </button></a>
        </ul>
      </div>
      <div class="body">
        {!!Form::model($client , ['route' => ['admin.clients.update' , $client->id] , 'method' => 'PATCH']) !!}
        @include('admin.clients.form')
        {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>


<!-- #END# Basic Validation -->
@endsection
