@extends('admin.layout.app')
@section('title')
تعديل الاعدادت

@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2> الاعدادت </h2>
                <ul class="header-dropdown m-r--5">
                </ul>
            </div>
            <div class="body">
                {!!Form::model($setting , ['route' => ['admin.settings.update' , $setting->id] , 'method' => 'PATCH','files'=>true]) !!}


                <div class="col-sm-6 col-xs-12  pull-right">
                    <div class="form-group form-float">
                        <label class="form-label">الإسم</label>
                        <div class="form-line">
                            {!! Form::text("name",null,['class'=>'form-control','placeholder'=>' اسم المؤسسه '])!!}
                        </div>
                    </div>
                </div>


                <div class="col-sm-6 col-xs-12  pull-right">
                    <div class="form-group form-float">
                        <label> رقم الجوال</label>
                        <div class="form-line">
                            {!! Form::text("phone",null,['class'=>'form-control','placeholder'=>' رقم الجوال '])!!}

                        </div>
                    </div>
                </div>
                
                    <div class="col-sm-6 col-xs-12  pull-right">
                        <div class="form-group form-float">
                            <label> العنوان </label>
                            <div class="form-line">
                                {!! Form::text("address",null,['class'=>'form-control','placeholder'=>'العنوان '])!!}
                            </div>
                        </div>
                    </div>

                <div class="col-sm-6 col-xs-12  pull-right">
                    <div class="form-group form-float">
                        <label> صورة الشعار</label>
                        <div class="form-line">
                            {!! Form::file("image",null,['class'=>'form-control'])!!}
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-12  pull-right">
                    <div class="form-group form-float">
                        <img src="{{getimg( $setting->image)}}" style="width:100px; height:100px">

                    </div>
                </div>



                <div class="col-xs-12">
                    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>
                </div>

                {!!Form::close() !!}
            </div>
        </div>
    </div>
</div>


<!-- #END# Basic Validation -->
@endsection
