@extends('admin.layout.app')

@section('title')
  عرض  العملاء
@endsection
@section('header')
  {{Html::style('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}
@endsection
@section('content')
  <!-- Exportable Table -->
  <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>
            عرض  العملاء
          </h2>
          <ul class="header-dropdown m-r--5">
            <a href="{{route('admin.clients.create')}}">   <button class="btn btn-success">إضافة عميل جديد</button></a>
          </ul>
        </div>
        <div class="body">
          <table class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
            <tr>
              <th>الإسم</th>
              <th>رقم الهويه</th>
              <th> جهه الصدور</th>
              <th>تاريخ الصدور</th>
              <th> الوظيفة</th>
              <th> العنوان</th>
              <th>اسم الكفيل</th>
              <th> العقود</th>
              <th>العمليات</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
              <th>الإسم</th>
              <th>رقم الهويه</th>
              <th> جهه الصدور</th>
              <th>تاريخ الصدور</th>
              <th> الوظيفة</th>
              <th> العنوان</th>
              <th>اسم الكفيل</th>
              <th> العقود</th>

              <th>العمليات</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($clients as $client)
              <tr>
                <td>{{$client->name}}</td>
                <td>{{$client->national_id}}</td>
                <td>{{$client->origin}}</td>
                <td>{{$client->date}}</td>
                <td>{{$client->job}}</td>
                <td>{{$client->address}}</td>

                <td>
                  @if (isset($client->guarantor_id))
                    {{$client->guarantor->name}}
                  @endif
                  ----
                </td>

                <td>
                  <a href="{{route('admin.clients.show',['id'=>$client->id])}}" class="btn btn-primary btn-circle"><i style="padding-top:5px;padding-left: 6px;" class="fa fa-money"></i></a>
                </td>
                <td>
                  <a href="{{route('admin.clients.edit',['id'=>$client->id])}}" class="btn btn-info btn-circle"><i style="padding-top:5px;padding-left: 6px;" class="fa fa-pencil"></i></a>
                 @if (Auth::user()->delete_client==1)
                    <a href="#"  onclick="Delete({{$client->id}})"  data-original-title="حذف" class="btn btn-danger btn-circle"><i style="padding-top: 5px;padding-left: 4px;" class="fa fa-trash-o"></i></a>
                    {!!Form::open( ['route' => ['admin.clients.destroy',$client->id] ,'id'=>'delete-form'.$client->id, 'method' => 'Delete']) !!}
                    {!!Form::close() !!}
                 @endif


                </td>

              </tr>
            @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- #END# Exportable Table -->

@endsection

@section('footer')
  <script>
    function Delete(id) {
      var item_id=id;
      console.log(item_id);
      swal({
        title: "هل أنت متأكد ",
        text: "هل تريد حذف هذا العميل ؟",
        icon: "warning",
        buttons: ["الغاء", "موافق"],
        dangerMode: true,

      }).then(function(isConfirm){
        if(isConfirm){
          document.getElementById('delete-form'+item_id).submit();
        }
        else{
          swal("تم االإلفاء", "حذف  المستخدم تم الغاؤه",'info',{buttons:'موافق'});
        }
      });
    }
  </script>
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/jszip.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js')!!}
@endsection


@section('data-table')
  <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>

  <script type="text/javascript"
          src="{{asset('admin/js/plugins/tables/datatables/datatables.min.js')}}"></script>
  <script type="text/javascript"
          src="{{asset('admin/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/js/plugins/forms/selects/select2.min.js')}}"></script>
  <script type="text/javascript"
          src="{{asset('admin/js/plugins/datatables_extension_buttons_init.js')}}"></script>
@endsection