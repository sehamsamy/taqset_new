@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<input type="hidden" name="id" value="@isset($user) {!! $user->id !!} @endisset">
<div class="col-sm-6 col-xs-12  pull-right">
    <div class="form-group form-float">
  <label class="form-label">الإسم</label>
  <div class="form-line">
    {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'  الاسم  '])!!}
  </div>
</div>
</div>


<div class="col-sm-6 col-xs-12  pull-right">
<div class="form-group form-float">
    <label > رقم الهوية</label>
    <div class="form-line">
            {!! Form::text("national_id",null,['class'=>'form-control','placeholder'=>'  رقم الهويه  '])!!}

    </div>
</div>
</div>

<div class="col-sm-6 col-xs-12  pull-right">
    <div class="form-group form-float">
        <label > جهة صدورها</label>
        <div class="form-line">
                    {!! Form::text("origin",null,['class'=>'form-control','placeholder'=>'  جهة الصدور  '])!!}

        </div>
    </div>
</div>

<div class="col-sm-6 col-xs-12  pull-right">
    <div class="form-group form-float">
        <label> تاريخ الاصدار</label>
        {{--<input type="text" name="date" class=" form-control datepicker-hijri_1">--}}

        <div class="form-line">
                    {!! Form::text("date",null,['class'=>'form-control datepicker-hijri_1'])!!}

        </div>
    </div>
</div>

<div class="col-sm-6 col-xs-12  pull-right">
    <div class="form-group form-float">
        <label >الجوال</label>
        <div class="form-line">
                    {!! Form::text("phone",null,['class'=>'form-control','placeholder'=>'الجوال  '])!!}

        </div>
    </div>
</div>


<div class="col-sm-6 col-xs-12  pull-right">
    <div class="form-group form-float">
        <label >العنوان</label>
        <div class="form-line">
                    {!! Form::text("address",null,['class'=>'form-control ','placeholder'=>'العنوان  '])!!}

        </div>
    </div>
</div>


<div class="col-sm-6 col-xs-12  pull-right">
    <div class="form-group form-float">
        <label >  الوظيفة</label>
        <div class="form-line">
                    {!! Form::text("job",null,['class'=>'form-control','placeholder'=>'  الوظيفه  '])!!}

        </div>
    </div>
</div>
<div class="col-sm-6 col-xs-12  pull-right">
<div class="form-group form-float">
    <label class="form-label" >الكفيل </label>
    {!! Form::select("guarantor_id",$guarantors,null,['class'=>'form-control js-example-basic-single','placeholder'=>'اختر الكفيل   '])!!}
</div>
</div>










<div class="col-xs-12">
    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>
</div>



@section('footer')



    <script type="text/javascript" src="{{asset('/admin/hijri-calender/js/jquery.calendars.js')}}"></script>

    <script type="text/javascript" src="{{asset('/admin/hijri-calender/js/jquery.calendars.plus.js')}}"></script>
    <script type="text/javascript" src="{{asset('/admin/hijri-calender/js/jquery.plugin.js')}}"></script>
    <script type="text/javascript" src="{{asset('/admin/hijri-calender/js/jquery.calendars.picker.js')}}"></script>

    <script type="text/javascript" src="{{asset('/admin/hijri-calender/js/jquery.calendars.ummalqura.js')}}"></script>
    <script>
        (function ($) {
            $.calendars.calendars.ummalqura.prototype.regionalOptions['ar'] = {
                name: 'Islamic',
                epochs: ['BAM', 'AM'],
                monthNames: ['محرّم', 'صفر', 'ربيع الأول', 'ربيع الآخر أو ربيع الثاني', 'جمادى الاول', 'جمادى الآخر أو جمادى الثاني',
                    'رجب', 'شعبان', 'رمضان', 'شوّال', 'ذو القعدة', 'ذو الحجة'],
                monthNamesShort: ['محرّم', 'صفر', 'ربيع الأول', 'ربيع الآخر أو ربيع الثاني', 'جمادى الاول', 'جمادى الآخر أو جمادى الثاني',
                    'رجب', 'شعبان', 'رمضان', 'شوّال', 'ذو القعدة', 'ذو الحجة'],
                dayNames: ['اح', 'اث', 'ث', 'ار', 'خ', 'ج', 'س'],
                dayNamesShort: ['اح', 'اث', 'ث', 'ار', 'خ', 'ج', 'س'],
                dayNamesMin: ['اح', 'اث', 'ث', 'ار', 'خ', 'ج', 'س'],
                digits: $.calendars.substituteDigits(['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩']),
                dateFormat: 'yyyy-mm-dd',
                firstDay: 6,
                isRTL: true
            };
        })(jQuery);
        $(function () {

            var calendar = $.calendars.instance('ummalqura', 'ar');
            $('.datepicker-hijri_1').calendarsPicker({
                calendar: calendar,
                defaultDate: '-1d',

                onSelect: function (date) {
                    var selectHijriDate = date[0];
                    //  convertToGeorgian(selectHijriDate.toString(),"national_id_date"); // ajax request

                }
            });


        });
    </script>





@endsection