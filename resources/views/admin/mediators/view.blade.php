@extends('admin.app')

@section('title')
 معلومات المدير{{ $user->name }}
@endsection
@section('header')
  {{Html::style('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2> معلومات عن {{ $user->name }}</h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{url('dashboard/sa/users')}}">   <button class="btn btn-danger">كل المديرين</button></a>
         </ul>
      </div>
      <div class="body">
        <div class="form-group form-float">
          <label class="form-label">الإسم</label>
          <div class="form-line">
            <input type ="text" value="{{$user->name}}" class="form-control" disabled>
          </div>
        </div>

        <div class="form-group form-float">
          <label class="form-label">الإيميل</label>
          <input type ="email" value="{{$user->email}}" class="form-control" disabled>
          <div class="form-line">
          </div>
        </div>


        <div class="form-group">

          <input type="radio" name="role" id="male" value="2" class="with-gap"<?php if(isset($user)) if($user->role== "2" ) { echo 'checked="checked"'; }?> disabled>
          <label for="male" class="m-l-20">مدير الموقع</label>

          <input type="radio" name="role" id="female" value="0" class="with-gap"<?php if(isset($user)) if($user->role== "0" ) { echo 'checked="checked"'; }?> disabled>
            <label for="female">مدخل بيانات</label>

            <input type="radio" name="role" id="other" value="1" class="with-gap"<?php if(isset($user)) if($user->role== "1" ) { echo 'checked="checked"'; }?> disabled>
            <label for="other" class="m-l-20">مشرف عام</label>

            <input type="radio" name="role" id="others" value="3" class="with-gap"<?php if(isset($user)) if($user->role== "3" ) { echo 'checked="checked"'; }?> disabled>
            <label for="others" class="m-l-20">بلوجر</label>

        </div>


      </div>
    </div>
  </div>
</div>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="body">
          <table class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
              <tr>
                <th>الإسم</th>
                <th>الايميل</th>
                <th>تاريخ العضوية</th>
                <th>الصﻻحيات</th>
              </tr>
            </thead>
          <tfoot>
            <tr>
              <th>الإسم</th>
              <th>الايميل</th>
              <th>تاريخ العضوية</th>
              <th>الصﻻحيات</th>
            </tr>
          </tfoot>
          <tbody>

            <tr>
              <td>{{$user->name}}</td>
              <td>{{$user->email}}</td>
              <td>{{$user->created_at}}</td>
              <td>
                @if($user->role ==0)
                <span class="badge badge-info">مدخل بيانات</span>
                @elseif($user->role ==1)
                <span class="badge badge-info">مشرف عام</span>
                @elseif($user->role == 2)
                <span class="badge badge-info">مدير الموقع</span>
                @else
                <span class="badge badge-info">بلوجر</span>
                @endif
              </td>
            </tr>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- #END# Exportable Table -->

<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>
          النشاطات
        </h2>
      </div>
      <div class="body">
          <table class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
              <tr>
                <th>النشاط</th>
                <th></th>
              </tr>
            </thead>
          <tfoot>
            <tr>
              <td>النشاط</td>
              <td></td>
            </tr>
          </tfoot>
          <tbody>
            @foreach($logs as $log)
            <tr>
              <td>{{$log->action}}</td>
              <td>{{$log->created_at}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- #END# Exportable Table -->
<!-- #END# Basic Validation -->
@endsection

@section('footer')
  {!!Html::script('admin/plugins/jquery-datatable/jquery.dataTables.js')!!}
  {!!Html::script('admin/js/pages/tables/jquery-datatable.js')!!}

  {!!Html::script('admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/jszip.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js')!!}
@endsection
