@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="col-sm-6 col-xs-12">
    <div class="form-group form-float">
  <label class="form-label">الإسم</label>
  <div class="form-line">
    {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'  الاسم  '])!!}
  </div>
</div>
</div>



<div class="col-sm-6 col-xs-12">
<div class="form-group form-float">
    <label > رقم الهوية</label>
    <div class="form-line">
            {!! Form::text("national_id",null,['class'=>'form-control','placeholder'=>'  رقم الهويه  '])!!}

    </div>
</div>
</div>






<div class="col-xs-12">
    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>
</div>

