@extends('admin.layout.app')

@section('title')
  عرض  الكفلاء
@endsection
@section('header')
{{Html::style('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}
@endsection
@section('content')
<!-- Exportable Table -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>
          عرض  الكفلاء
        </h2>
        <ul class="header-dropdown m-r--5">
          <a href="{{route('admin.guarantors.create')}}">   <button class="btn btn-success">إضافة كفيل جديد</button></a>
</ul>
        </div>
        <div class="body">
          <table class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
              <tr>
                <th>الإسم</th>
                <th>رقم الهويه</th>
                <th> جهه الصدور</th>
                <th>تاريخ الصدور</th>
                <th>مكان العمل</th>
                <th>العمليات</th>
              </tr>
            </thead>
          <tfoot>
            <tr>
              <th>الإسم</th>
              <th>رقم الهويه</th>
              <th> جهه الصدور</th>
              <th>تاريخ الصدور</th>
              <th>مكان العمل</th>
              <th>العمليات</th>
            </tr>
          </tfoot>
          <tbody>
            @foreach($guarantors as $guarantor)
            <tr>
              <td>{{$guarantor->name}}</td>
              <td>{{$guarantor->national_id}}</td>
              <td>{{$guarantor->origin}}</td>
              <td>{{$guarantor->date}}</td>
              <td>{{$guarantor->work_address}}</td>

              <td>
                 <a href="{{route('admin.guarantors.edit',['id'=>$guarantor->id])}}" class="btn btn-info btn-circle"><i  class="fa fa-pencil"></i></a>
                <a href="#"  onclick="Delete({{$guarantor->id}})"  data-original-title="حذف" class="btn btn-danger btn-circle"><i class="fa fa-trash-o"></i></a>
                {!!Form::open( ['route' => ['admin.guarantors.destroy',$guarantor->id] ,'id'=>'delete-form'.$guarantor->id, 'method' => 'Delete']) !!}
                {!!Form::close() !!}

              </td>

            </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- #END# Exportable Table -->

@endsection

@section('footer')
  <script>
    function Delete(id) {
      var item_id=id;
      console.log(item_id);
      swal({
        title: "هل أنت متأكد ",
        text: "هل تريد حذف هذا الكفيل ؟",
        icon: "warning",
        buttons: ["الغاء", "موافق"],
        dangerMode: true,

      }).then(function(isConfirm){
        if(isConfirm){
          document.getElementById('delete-form'+item_id).submit();
        }
        else{
          swal("تم االإلفاء", "حذف  المستخدم تم الغاؤه",'info',{buttons:'موافق'});
        }
      });
    }
  </script>
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/jszip.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')!!}
  {!!Html::script('admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js')!!}
@endsection


@section('data-table')
   <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>

    <script type="text/javascript"
            src="{{asset('admin/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('admin/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('admin/js/plugins/datatables_extension_buttons_init.js')}}"></script>
@endsection