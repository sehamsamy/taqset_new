@extends('admin.layout.app')
@section('title')
تعديل الكفيل
{{ $guarantor->name }}
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>تعديل الكفيل      {{ $guarantor->name }}</h2>
        <ul class="header-dropdown m-r--5">
          <a href="{{route('admin.guarantors.index')}}">    <button class="btn btn-danger">كل الكفلاء </button></a>
        </ul>
      </div>
      <div class="body">
        {!!Form::model($guarantor , ['route' => ['admin.guarantors.update' , $guarantor->id] , 'method' => 'PATCH']) !!}
        @include('admin.guarantors.form')
        {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>


<!-- #END# Basic Validation -->
@endsection


