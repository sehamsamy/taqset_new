@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="col-sm-6 col-xs-12 pull-right">
    <div class="form-group form-float">
  <label class="form-label">الإسم</label>
  <div class="form-line">
    {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'  الاسم  '])!!}
  </div>
</div>
</div>



<div class="col-sm-6 col-xs-12 pull-right">
    <div class="form-group form-float">
        <label> تاريخ المخالصه</label>
        <div class="form-line">
                    {!! Form::date("date",null,['class'=>'form-control'])!!}

        </div>
    </div>
</div>



<div class="col-sm-6 col-xs-12 pull-right">
    <div class="form-group form-float">
        <label class="form-label" >العميل </label>
        {!! Form::select("client_id",$clients,null,['class'=>'form-control js-example-basic-single','placeholder'=>'اختر العميل   '])!!}
    </div>
</div>



<div class="col-xs-12">
    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>
</div>


@section('footer')


@endsection