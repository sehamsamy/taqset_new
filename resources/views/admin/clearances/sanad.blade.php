
 <!DOCTYPE html>
 <html lang="en">
 <head>
   <title>سند مخالصة عامة</title>
   <link rel="stylesheet" href="css/bootstrap.min.css">
 </head>
 <style>
   body {
     width: 100%;
     height: 100%;
     margin: 0;
     padding: 0;
     background-color: #FAFAFA;
     font: 12pt "Tahoma";
     direction: rtl;
   }
   * {
     box-sizing: border-box;
     -moz-box-sizing: border-box;
   }
   .page {
     width: 210mm;
     min-height: 297mm;
     padding: 10mm 0mm;
     margin: 5mm;
     border: 10px #f00 solid;
     border-radius: 5px;
     background: white;
     box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
   }
   .subpage {
     padding: 0.5cm;
     border: 5px black solid;
     height: 282mm;
     outline: 0cm;
   }
   .subpage h2 {
     text-align: center;
     width: 100%;
     display: inline-block
   }
   /**  /////////////////////////////  REEM ////////////////////////////////////// */
   .head {
     border-bottom: 2px black solid;
     display: flex;
   }
   .logo {
     width: 80px;
     height: 80px;
     object-fit: contain;
   }
   .content-top {
     margin: 30px 0 0 0;
   }
   .date {
     font-weight: bold;
   }
   .right {
     width: 50%;
   }
   .p-right-width {
     width: 220px;
     line-height: 1.8;
     height: 60px;
     text-align: center;
   }
   .left {
     width: 50%;
   }
   .left {
     float: left;
     text-align: center;
   }
   .p-left {
     text-align: left;
   }
   .p-inline {
     display: inline-block;
   }
   .p-width {
     min-width: 50%;
     float: left;
   }
   .content {
     margin-top: 20px;
   }
   .date {
     width: 50px;
     display: inline-block;
   }
   .text-center {
     text-align: center;
     margin: 0 auto;
   }
   .content h3{
     border-bottom: 1px solid;
     width: max-content;
   }
   p {
     line-height: 1.5;
   }
   .btm{
     width: 100%;
     min-height: 30px;
     display: block;
   }
   .bold {
     font-weight: bold;
   }
   .foot {
     margin-top: 30px;
   }
   .spann {
     display: inline-block;
     min-width: 150px;
     text-align: center;
   }
   .width-right {
     width: 62px !important;
     text-align: right;
     display: inline-block;
     font-weight: bold;
   }
   .margin {
     margin: 30px auto 10px auto;
   }
   .long{
     width: 100%;
     display: block;
     margin: 10px 0 0 0;
   }
   .sml{
     min-width: 50px;
   }
   /* Table */
   table,
   td,
   th {
     border: 1px solid black;
     text-align: center;
   }
   table {
     border-collapse: collapse;
     width: 100%;
     margin: 20px auto 0 auto;
   }
   th,
   td {
     height: 40px;
   }
   td{
     color: #b33133;
   }
   th {
     width: 160px;
   }
   @page {
     size: A4;
     margin: 0;
   }
   @media print {
     html,
     body {
       width: 210mm;
       height: 297mm;
     }
     .page {
       margin: 0;
       border: initial;
       border-radius: initial;
       width: initial;
       min-height: initial;
       box-shadow: initial;
       background: initial;
       page-break-after: always;
     }
     .info_date{
       float: left;
     }


     .info_date>p{
       margin: 2px;

     }
   }

     /******************* MODIFICATIONS *******************************************/
   .info{
       display: inline-block;
       width: 100%;
       padding: 10px 0;
   }
     .info_about{
         width: 50%;
         float: right;
         display: inline-block;
     }
     .info-left{
         width: 50%;
         float: left;
         display: inline-block;
         text-align: center;
     }
     p{
         margin: 0 0 5px 0;
     }
     /*********************************/

 </style>
 <body>
 <div class="book">


   <div class="page">
     <div class="subpage">
       <div class="content">
         <center style="">بسم الله الرحمن الرحيم</center>
         <div class="info">
           <div class="info_about">
             <p >المملكه العربيه السعوديه</p>
             <p> القصيم-بريدة</p>
             <p>عبدالرحمن رجاءالعنزي  </p>
             <p>للتقسيط </p>
             <p>سيارات -بطاقات سوا </p>
               <p>جوال:0504895942</p>

           </div>

             <div class="info-left">
                 <p>
                     <span class="bold"> التاريخ :-</span>
                     <span class="dates">
                            09-06-1400 هـ
                        </span>
                 </p>
                 <p>
                     <span class="bold"> الموافق :-</span>
                     <span class="dates">
                   {{$clearance->date}}
                        </span>
                 </p>
             </div>

         </div>
           <h3 class="text-center bold">سند مخالصة عامة</h3>
           <div class="content-top">



                   <p>
                       <span class="bold"> رقم المخالصه :-</span>
                       <span >
                            {{$clearance->randam_num}}
                        </span>
                   </p>
           </div>
           <p class="bold">
             أنا الموقع أدناه :-
             <span class="spann"> {{$clearance->name}}</span>
           </p>
           <p class="bold">
             أقر بجميع الضمانات الفعلية والقانونية بأننى قد استلمت
             <span class="btm">
                        من :-
                          <span class="spann"> {{$clearance->client->name}}</span>
                      </span>
           </p>
           <p class="bold">
             جميع المبالغ والمطالبات المالية ولم يعد لى أى حق أو ادعاء أو المطالبة بهذا الخصوص وبناءا عليه يعتبر هذا السند ابراء لذمة العميل .
           </p>

           <div class="foot text-center">
             <div class="left">
               الموظف المختص :-
               <p class="btm">
                 {{$clearance->name}}
               </p>
               التوقيع :-
               <span class="spann"></span>
             </div>
           </div>

       </div>
     </div>
     <script src="js/bootstrap.min.js"></script>

     <script type="text/javascript">
       window.print();
     </script>
   </div>
   </div>
 </body>
 </html>
