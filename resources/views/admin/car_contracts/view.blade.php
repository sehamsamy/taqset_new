@extends('admin.layout.app')

@section('title')
    عرض  الاقساط
@endsection
@section('header')
    {{Html::style('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}
@endsection
@section('content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        عرض  الاقساط
                    </h2>

                </div>
                <div class="body">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>تاريخ القسط</th>
                            <th>المبلغ المستحق </th>
                            <th>سداد </th>
                            <th> طباعه </th>

                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>تاريخ القسط</th>
                            <th>المبلغ المستحق </th>
                            <th>سداد </th>
                            <th> طباعه </th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($qests as  $key=>$qest)
                            <tr>

                                <td>{{++$key}}</td>
                                <td>{{$qest->date}}</td>
                                <td>{{floor($qest->one_pay)}}</td>

                        @if ($qest->pay==0)

                                    <td>
                                        {{--<a href="{{route('admin.car_contracts.sadad',['id'=>$qest->id])}}"  class="btn btn-primary btn-circle"><i style="padding-top:5px;padding-left: 6px;" class="fa fa-money"></i></a>--}}
                                        <a href="#"  onclick="myFunction({{$qest->id}})"  data-toggle="tooltip" class="btn btn-primary btn-circle" ><i style="padding-top:5px;padding-left: 6px;" class="fa fa-money"></i></a>

                                        {!!Form::open( ['route' => ['admin.car_contracts.sadad',$qest->id] ,'id'=>'reason-form'.$qest->id, 'method' => 'get']) !!}
                                        <input type="hidden" name="" value="{{$qest->one_pay}}" id="one_pay">
                                        <input type="hidden" name="key" value="{{$key}}">

                                        {!!Form::close() !!}

                                    </td>
                            @else
                            <td> تم السداد</td>

                                @endif
                                <td>

                                    {!!Form::open( ['route' => ['admin.car_contracts.sadad_qests',$qest->id] , 'method' => 'post']) !!}
                                    <input type="hidden" name="key" value="{{$key}}">

                                    <button class="btn btn-info " type="submit"><i style="padding-top:5px;padding-left: 6px;" class="fa fa-print"></i></button>
                                    {!!Form::close() !!}
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@section('footer')



    <script>
        function myFunction(id) {
            var item_id=id;
           var itemx= document.getElementById('one_pay');
      var value=  itemx.value;

            swal({


                title: "   سداد القسط",
                text: value +"هل  تريد سداد هذا القسط وقيمه المبلغ ؟",

                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function (isConfirm ) {
                if (isConfirm) {
                    console.log(item_id);
                    $('#ff').val($('#swal').val());
                    document.getElementById('reason-form'+item_id).submit();


                } else {
                    swal("تم االإلفاء", " سداد القسط  تم الغاؤه", 'info', {buttons: 'موافق'});
                }


            });


        }

    </script>
@endsection


@section('data-table')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>

    <script type="text/javascript"
            src="{{asset('admin/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('admin/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('admin/js/plugins/datatables_extension_buttons_init.js')}}"></script>
@endsection