
<?php
         if ($contract->kind=="client")
            {
                $id=$contract->clientv2_id;

                  $secound=\App\Client::find($id);

          }
    else
    {
    $id=$contract->guarantor_id;
        $secound=\App\Guarantor::find($id);

   }



   $months_count=$contract->months_count;
$start_date=$contract->start_date;

$end_date=\Carbon\Carbon::parse($start_date)->addMonths($months_count-1);
$all=ceil($contract->amount+$contract->added_value+$contract->paid_amount);

$end=$end_date->format('Y-m-d');

?>

<!DOCTYPE html>

<html dir="rtl">

<head>
    <script>

       window.onload= window.print();
    </script>


    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>برنامج التقسيط </title>

    <!-- Tell the browser to be responsive to screen width -->

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">




    <link rel="stylesheet" href="dist/css/skins/_all-skins-rtl.min.css">

    <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">


    <!-- Bootstrap 3.3.6 -->
    <style>

        span{
            color:blue;
        }

        .info>p{
            margin: 2px;

        }
        p{
            margin: 15px;

        }
        .info_date{
            float: left;
            margin-left:300px;

        }
        .info_date>p{
            margin: 2px;

        }

        /***************** MODIFICATIONS ************************/
        .info-right{
            width: 50%;
            float: right;
            display: inline-block;
        }
        .info_date{
            width: 50%;
            display: inline-block;
            margin: 0;
            text-align: center;
        }
        .info-right p {
            margin: 0;
        }
        .info {
            line-height: 1.5;
            display: inline-block;
            width: 100%;
            margin: 15px 0 0 0;
        }
        .ititle{
            text-align: center;
            color: firebrick;
            margin: 0;
        }
        .idi{
            text-align: left;
            margin: 0;
        }
        .content p {
            margin: 0;
        }
        .content{
            line-height: 1.6;
        }
        .block1{
            width: 100%;
            text-align: center;
        }
        .block1 p{
            display: inline-block;
            min-width: 100px;
        }
        .header_w{
            margin-right: 25px;
        }
        /******************************************/

    </style>


</head>
<!-- Content Wrapper. Contains page content -->
<body>
<div class="content-wrapper" >
    <div style=" padding:20px; border-style: solid;">

        <!-- Content Header (Page header) -->

        <section class="content-header">
            <center style="">بسم الله الرحمن الرحيم</center>
            <div class="info">

            <div class="info-right">
                <p >المملكه العربيه السعوديه</p>
                <p><div class="header_w">القصيم-بريدة</div></p>
                <p>عبدالرحمن رجاء العنزي  </p>
                <p><div class="header_w">للتقسيط</div></p>
                <p>سيارات -بطاقات سوا </p>
                <p>جوال:0504895942</p>

            </div>

                <div class="info_date">

                    <p>بتاريخ: <span> {{$today_date}}</span></p>
                    <p>الموافق: <span> {{$date}}</span></p>

                </div>
            </div>

            <h2 class="ititle">العنزي للتقسيط</h2>


            <p class="idi">No: {{$contract->id}}</p>

            <hr style="color:lightgray">
        </section>

        <!-- Main content -->

        <section class="content"style="text-align:center">
            <center> <u>عقد اتفاق بيع بالتقسيط</u></center>
            <table>
                <tr>
                    <p>تم الاتفاق بين الطرفين فى يوم <span> {{$dayname}}	&nbsp;
            &nbsp;</span> الموافق<span> 	{{$date}}&nbsp;</span> هـ</p>

                </tr>
                <tr>
                    <p style=" display:inline">الطرف الاول/<span> {{$contract->mediator->name}}	</span></p>


                    <p style="display:inline">رقم الهويه/<span>  {{$contract->mediator->national_id}}	&nbsp;&nbsp;</span></p>
                </tr>
                <tr>
                    <p style=" display:inline">الطرف التانى /<span> {{$contract->client->name}}	&nbsp;</span></p>
                    <p style="display:inline">رقم الهويه/<span> {{$contract->client->national_id}}</span></p>
                    <p> بتاريخها<span> 	&nbsp;{{$contract->client->date}}	&nbsp;</span>ومصدرها <span>{{$contract->client->origin}}</span></p>
                </tr>
                <tr>
                    <p>انه قد باع الطرف  الاول على الطرف الثانى  سياره<span> 	{{$contract->car_name}}	&nbsp;</span>رقم الشاص<span> {{$contract->shas_num}}</span> </p>

                    <p> رقم اللوحه <span>{{$contract->car_num}} </span>&nbsp;&nbsp;لونها <span>{{$contract->color}}</span>&nbsp;&nbsp;موديلها<span> {{$contract->model}}</span></p>


                    <p>بمبلغ قدره<span> 	&nbsp;{{$all}}	&nbsp;</span>ريال ,وكتابيا &nbsp;<span>{{$total_all}}</span></p>
                </tr>

                <tr>
                    <div>
                        <p>ويدفع الطرف الثانى  مبلغ وقدره<span> &nbsp;{{$contract->paid_amount }}&nbsp;</span>ريال ,وكتابيا &nbsp;<span>{{$total}}</span>&nbsp;عند كتابه العهد</p>
                        <p>&nbsp;على ان يتم السداد الباقى باقساط شهريه كل قسط قدره<span> &nbsp;{{$payment->one_pay}} 	&nbsp;</span>ريال ,وكتابيا &nbsp;<span>{{$one_pay}}</span></p>
                        <p> تبدا الاقساط من يوم<span>{{$contract->start_date}}&nbsp;</span> وتنتهى  فى يوم. <span> {{$end}}&nbsp;	&nbsp;</span></p>
                    </div>
                </tr>
                <tr>
                    <p> واتعهد انا المشترى بان ادفع الاقساط فى مواعيدها المحدده وبدون تاخير او مماطله وفى حاله تاخيرى عن السداد لمدة شهرين متتالين</p>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;او متفرقه يعتبر المبلغ  حالا بكامله للبائع والتزم بسداده كاملا.</p>
                <!-- <tr>
                </tr>
                    <p> رقم اللوحه <span>{{$contract->car_num}} </span>&nbsp;&nbsp;لونها <span>{{$contract->color}}</span>&nbsp;&nbsp;موديلها<span> {{$contract->model}}</span></p>


                    <p>بمبلغ قدره<span> 	&nbsp;{{$contract->amount+$contract->added_value+$contract->paid_amount}}	&nbsp;</span>ريال ,وكتابيا &nbsp;<span>{{$total_all}}</span></p>
                </tr>

                <tr>
                    <div>
                        <p>ويدفع الطرف الثانى  مبلغ وقدره<span> &nbsp;{{$contract->paid_amount }}&nbsp;</span>ريال ,وكتابيا &nbsp;<span>{{$total}}</span>&nbsp;عند كتابه العهد</p>
                        <p>&nbsp;على ان يتم السداد الباقى باقساط شهريه كل قسط قدره<span> &nbsp;{{$payment->one_pay}} 	&nbsp;</span>ريال ,وكتابيا &nbsp;<span>{{$one_pay}}</span></p>
                        <p> تبدا الاقساط من يوم<span>{{$contract->start_date}}&nbsp;</span> وتنتهى  فى يوم. <span> {{$end}}&nbsp;	&nbsp;</span></p>
                    </div>
                </tr>
                <tr>
                    <p> واتعهد انا المشترى بان ادفع الاقساط فى مواعيدها المحدده وبدون تاخير او مماطله وفى حاله تاخيرى عن السدادلمدده شهربن ممتاليه</p>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;او متفرقه يعتبر المبلغ  حالابكامله للبائع والتزم بسداده كاملا.</p>
                <tr>
                </tr> -->


                <center> <u><p style="color:firebrick">
                            تعهد الكفيل
                        </p></u></center>
                <div>
                    <p>اقر انا المدعو/<span> {{$secound->name}}	&nbsp;</span> سعودى الجنسيه بموجب بطاقه رقم<span> 	{{$secound->national_id}}&nbsp;</span></p>

                    <p> بتاريخها<span>{{$secound->date}} 	&nbsp;&nbsp;</span>ومصدرها <span> {{$secound->origin}}</span></p>
                    <p>ومقر عملى<span>{{$secound->work_address}}</span> </p>

                    <p> باننى اكفل الطرف الثانى كفاله غرامه واداء من  مالى الخاص فى اى مطالبه من الطرف الاول تجاه هذا العقد اذا تاخر مكفولى عن السداد </p>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;اى قسط من الاقساط  اكون ملتزم بيها امام الله وامام البائع دون الرجوع الى مكفولى...</p>

                    <p> وعلى ذلك اوقع</p>
                </div>


                <center> <u><p style="color:firebrick">
                            تعهد الكفيل
                        </p></u></center>
               <!--  <div>
                    <p>اقر انا المدعو/<span> {{$secound->name}}	&nbsp;</span> سعودى الجنسيه بموجب بطاقه رقم<span> 	{{$secound->national_id}}&nbsp;</span></p>

                    <p> بتاريخها<span>{{$secound->date}} 	&nbsp;&nbsp;</span>ومصدرها <span> {{$secound->origin}}</span></p>
                    <p>ومقر عملى<span>{{$secound->work_address}}</span> </p>

                    <p> باننى اكفل الطرف الثانى كفاله غرامه واداء من  مالى الخاص فى اى مطالبه من الطرف الاول تجاه هذا العقد اذا تاخر مكفولى عن السداد </p>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;اى قسط من الاقساط  اكون ملتزم بيها امام الله وامام البائع دون الرجوع الى مكفولى...</p>

                    <p> وعلى ذلك اوقع</p>
                </div> -->
                <div style="display: flex;justify-content: space-between;align-items: center">

                    <div>

                        <p style="">  الطرف الأول </p>
                        <p> <span> {{$contract->mediator->name}} 	</span></p>
                        <p style="margin-top: 20px"> الشاهد الأول </p>

                    </div>
                    <div>

                        <p > الطرف الثانى </p>
                        <p > <span> {{$contract->client->name}}	</span></p>
                        <p style="margin-top: 20px"> الشاهد الثانى </p>
                    </div>

                </div>




                <!-- Your Page Content Here -->

                <div class="clearfix"></div>

        </section>




        <footer class="main-footer">

            <!-- To the right -->

            <div class="pull-left hidden-xs">



            </div>

            <!-- Default to the left -->

        </footer>
    </div>
    <!-- /.content -->

</div>

<!-- /.content-wrapper -->

</body>

</html>
