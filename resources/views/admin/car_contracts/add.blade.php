@extends('admin.layout.app')

@section('title')
إضافة  عقد سياره
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>اضافه عقد سيارة</h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{route('admin.car_contracts.index')}}">    <button class="btn btn-danger">عقود السيارات </button></a>
        </ul>
      </div>
      <div class="body">
          {!!Form::open( ['route' => 'admin.car_contracts.store' ,'class'=>'form phone_validate', 'method' => 'Post','files' => true]) !!}
        @include('admin.car_contracts.form')
        {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>
<!-- #END# Basic Validation -->
@endsection


