@extends('admin.layout.app')

@section('title')
 تقرير
@endsection
@section('header')

    <style type="text/css">
        @media print
        {
            body * { visibility: hidden; }
            .row * { visibility: visible; }
            .row { position: absolute; top: 40px; left: 30px; }
        }
    </style>
    {{Html::style('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}
@endsection
@section('content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="col-3 pull-right">
                    <h2>
                         عرض  الاقساط للعقد {{$contract->id}}
                    </h2>
                    </div>
                    <div class="col-3 pull-left" style="">
                    <button class="btn btn-success" onclick=" window.print();"   > <i style="padding-top:5px;padding-left: 3px;" class="fa fa-print"></i></button>
                    </div>
                </div>
                <div class="body">
                    ;

                    <div class="clearfix"></div>

                    <div class="col-md-3 pull-right">
                        <label> الطرف الاول- الوسيط : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default">


                                {{$contract->mediator->name}}

                        </label>
                    </div>


                    <div class="col-md-3 pull-right">
                        <label> اسم الطرف الثانى : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default">


                            @if ($contract->kind=="client")
                                <?php
                                $id=$contract->clientv2_id;
                                $client=\App\Client::find($id);
                                ?>
                                {{$client->name}}
                               @else
                                <?php
                                $id=$contract->guarantor_id;
                                $guarantor=\App\Guarantor::find($id);
                                ?>
                               {{$guarantor->name}}
                            @endif
                        </label>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-3 pull-right">
                        <label> اسم السياره : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{$contract->car_name}}</label>
                    </div>




                    <div class="col-md-3 pull-right">
                        <label> لون  السياره : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{$contract->color}}</label>
                    </div>



                    <div class="clearfix"></div>

                    <div class="col-md-3 pull-right">
                        <label>رقم الشاص : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{$contract->shas_num}}</label>
                    </div>



                    <div class="col-md-3 pull-right">
                        <label>رقم اللوحه : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{$contract->car_num}}</label>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-md-3 pull-right">
                        <label>موديل السياره: </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{$contract->model}}</label>
                    </div>



                    <div class="clearfix"></div>

                    <div class="col-md-3 pull-right">
                        <label>  قيمة العقد : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{$contract->amount}}</label>
                    </div>

                    <div class="col-md-3 pull-right">
                        <label>  القيمه المضافه : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{$contract->added_value}}</label>
                    </div>


                    <div class="clearfix"></div>



                    <div class="col-md-3 pull-right">
                        <label>   المبلغ المستحق : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{$contract->amount+$contract->added_value+$contract->paid_amount}}</label>
                    </div>



                    <div class="col-md-3 pull-right">
                        <label>   المبلغ المدفوع : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{$contract->paid_amount}}</label>
                    </div>



                    <div class="clearfix"></div>

                    <div class="col-md-3 pull-right">
                        <label>   عدد الاقساط المدفوعه : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default">{{$paymemts_paid}}</label>
                    </div>


                    <div class="col-md-3 pull-right">
                        <label>   عدد الاقساط المتبقيه : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default">{{$paymemts_notpaid}}</label>
                    </div>


                    <div class="clearfix"></div>

                    <div class="col-md-3 pull-right">
                        <label>   ماتم تسديده : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{$paid}}</label>
                    </div>




                    <div class="col-md-3 pull-right">
                        <label>  متبقى : </label>
                    </div>
                    <div class="col-md-3 pull-right">
                        <label class="label label-default"> {{ceil($ss)}}</label>
                    </div>


                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>

                    <center>
                        <div class="box-header">

                            <h3 class="box-title">الاقساط المتبقية</h3>

                        </div>
                    </center>
                    <table  class="table table-bordered table-striped data-table">

                        <thead>

                        <tr>

                            <th>#</th>

                            <th>تاريخ القسط</th>

                            <th>قيمه القسط</th>

                        </tr>

                        </thead>

                        <tbody>

              @foreach($payments as $key=>$payment)
                  <tr>
                      <td>{{$key+1}} </td>
                  <td>{{$payment->date}} </td>
                  <td>{{ceil($payment->one_pay)}} </td>
                  </tr>
                  @endforeach
                        </tbody>

                        <tfoot>

                        <tr>

                            <th>#</th>
                            <th>تاريخ القسط</th>
                            <th>قيمه القسط</th>

                        </tr>

                        </tfoot>

                    </table>



                </div>

                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@section('footer')
    <script>
        function Delete(id) {
            var item_id=id;
            console.log(item_id);
            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذا المستخدم ؟",
                icon: "warning",
                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('delete-form'+item_id).submit();
                }
                else{
                    swal("تم االإلفاء", "حذف  المستخدم تم الغاؤه",'info',{buttons:'موافق'});
                }
            });
        }
    </script>
@endsection


@section('data-table')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>

    <script type="text/javascript"
            src="{{asset('admin/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('admin/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('admin/js/plugins/datatables_extension_buttons_init.js')}}"></script>
@endsection
